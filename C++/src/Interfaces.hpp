#ifndef Interfaces_H
#define Interfaces_H
#include <cmath>
#include <vector>
#include <list>
#include "Eigen"
#include <iostream>
#include <fstream>

#define TOLL 1e-6


using namespace std;
using namespace Eigen;

namespace Interfaces {

enum class TypeIntersection {
  NOTINT = 0,
  PROPERINT = 1,
  BEGINVERTEXINT = 2,
  ENDVERTEXINT = 3,
  ONLINE = 4

};

enum class onBoundingBox
{
    NOTINBb = 0,
    INBb = 1
};

class IPoint{

public:
    virtual ~IPoint() {}

    virtual void SetVertex(unsigned int v) = 0;

    virtual double GetX() const = 0;
    virtual double GetY() const = 0;

    virtual void SetX(double x) = 0;
    virtual void SetY(double y) = 0;

    virtual unsigned int GetVertex() const = 0;

    virtual void SetTypeIntersection(TypeIntersection typeint) = 0;
    virtual TypeIntersection GetTypeIntersection() const = 0;

    virtual void SetBbType(onBoundingBox Bbtype) = 0;
    virtual onBoundingBox GetBbType() const =0;

    inline friend bool operator<(const IPoint& point1,const  IPoint& point2)
    {
    if (abs(point1.GetX() - point2.GetX()) <= TOLL)
    {
        if(abs(point1.GetY() - point2.GetY()) <= TOLL)
            return false;
        else
            return point1.GetY() < point2.GetY();
    }
    else
        return point1.GetX() < point2.GetX();
    }
    static bool cmp(IPoint*& point1, IPoint*& point2) {return *point1 < *point2;}
    inline friend bool operator==(const IPoint& point1,const IPoint& point2) { return ((abs(point1.GetX() - point2.GetX()) < TOLL) && (abs(point1.GetY() - point2.GetY()) < TOLL));}
    inline friend bool operator!=(const IPoint& point1,const IPoint& point2) {return !(point1==point2);}
};

class Segment {
private:
IPoint* __begin;
IPoint* __end;
Vector2d __direction;
Vector2d __normalDirection;

public:
Segment() {__begin = NULL; __end = NULL;}
Segment(IPoint *p1, IPoint *p2);
Segment(const Segment& s1)
{
    __begin = s1.GetBegin();
    __end = s1.GetEnd();
    __direction = s1.GetDirection();
    __normalDirection = s1.GetNormalDirection();
}

IPoint* GetBegin() const { return __begin;}
IPoint* GetEnd() const { return __end;}

Vector2d GetDirection() const { return __direction;}
Vector2d GetNormalDirection() const {return __normalDirection;}
~Segment(){};

};

class IPolygon{
protected:
    vector<IPoint*> __pointsList;
    vector<IPoint*> __boundingBox;
    vector<Segment> __segmentsList;
public:
    IPolygon() {__pointsList.reserve(3); __boundingBox.reserve(4);}
    virtual ~IPolygon() {}


    double ComputeArea();

    const vector<IPoint*> GetPointsList() const { return __pointsList;}
    const vector<Segment> GetSegmentsList() const { return __segmentsList;}

    virtual bool PointInPolygon(const IPoint& p) const = 0;

    void Translate(double traslata_x, double traslata_y);
    vector<IPoint*> GetBoundingBox(){ __boundingBox.clear();  this->CreateBoundingBox();  return __boundingBox;}

    void ResetPointsIntersectionType() {for(unsigned int i=0; i<__pointsList.size(); i++)
            __pointsList[i]->SetTypeIntersection(TypeIntersection::NOTINT);}

private:
    void CreateBoundingBox();
};

class IPointFactory
{
public:
    virtual ~IPointFactory() {}
    virtual IPoint* CreatePoint(double x, double y) = 0;
};

class IPolygonFactory{
public:
    virtual ~IPolygonFactory(){}
     virtual IPolygon* CreatePolygon(vector<IPoint*> points,vector<unsigned int>& vertex, bool sort_vertex) = 0;


};
}


#endif // EMPTYCLASS_H
