#include "Geometryfunction.hpp"
#include "GeometryObject.hpp"
#include "Interfaces.hpp"

using namespace GeometryObject;

namespace GeometryLibrary {

float GeometryLibrary::IntersectionPoint::__tollParallelism = 1e-6;
float GeometryLibrary::IntersectionPoint::__tollTangent =1e-5;
IPoint* GeometryLibrary::IntersectionPoint::__found = NULL;

vector<IPolygon*> GeometryLibrary::Mesh::__polygonInMesh = {};

// verifichiamo che i segmenti non siano paralleli

bool GeometryLibrary::IntersectionPoint::ComputeParallelism(const Segment& edge, const Segment& line)
{
    float edgenorm = edge.GetDirection().squaredNorm();
    float linenorm = line.GetDirection().squaredNorm();

    // vero e proprio confronte delle direzioni

    if (abs(abs(edge.GetDirection()[0]/edgenorm) - abs(line.GetDirection()[0]/linenorm)) <__tollParallelism
        && abs(abs(edge.GetDirection()[1]/edgenorm) - abs(line.GetDirection()[1]/linenorm)) <__tollParallelism)
        return true;

    else
        return false;
}

// il metodo calcola l'intersezione tra i due segmenti in input

Interfaces::TypeIntersection GeometryLibrary::IntersectionPoint::ComputeIntersection(const Interfaces::Segment& edge, const Interfaces::Segment& line)
{

    Vector2d edgeDirection = edge.GetDirection();
    Vector2d lineDirection = line.GetDirection();

    // innanzitutto bisogna verificare che i due segmenti non siano paralleli

    if (ComputeParallelism(edge, line)== true)
        return TypeIntersection::NOTINT;

    // per calcolare l'intersezione dobbiamo risolvere il sistema lineare Ax=b;
    // calcoleremo però il vettore x così: x=Cb, dove C è l'inversa di A

    // creazione della matrice C

    Matrix2d A = Matrix2d::Zero();
    A << -lineDirection[1], lineDirection[0], -edgeDirection[1], edgeDirection[0];
    double detA = A.determinant();

    A = A/detA;
    // calcolo del vettore b

    IPoint* endEdge = edge.GetEnd();
    double xl = endEdge->GetX();
    double yl = endEdge->GetY();
    IPoint* endLine = line.GetEnd();
    double xr = endLine->GetX();
    double yr = endLine->GetY();

    Vector2d b = Vector2d::Zero();
    b << xl-xr, yl-yr;

    // risoluzione del sistema

    Vector2d x;
    x = A*b;
    //x = A.colPivHouseholderQr().solve(b);

    // assegnazione della tipologia d'intersezione, in base al valore ddell'ascissa curvilinea t

    // se 0<t<1, allora è un'intersezione PROPRIA

    if ( x[0] > __tollTangent && x[0] < 1 - __tollTangent)
    {
        PointFactory pointfac;
        __found = pointfac.CreatePoint(xl - (edgeDirection[0]) * x[0], yl - (edgeDirection[1]) * x[0]);
        __found->SetTypeIntersection(TypeIntersection::PROPERINT);
        return TypeIntersection::PROPERINT;
    }

    // se t=0, allora è un'intersezione AL VERTICE (in paticolare al vertice di fine)

    else if (abs(x[0]) < __tollTangent)
    {
        edge.GetEnd()->SetTypeIntersection(TypeIntersection::ENDVERTEXINT);
        return TypeIntersection::ENDVERTEXINT;
    }

    // se t=1, allora è un'intersezione AL VERTICE (in paticolare al vertice di inizio)

    else if (abs(x[0] - 1) < __tollTangent)
    {
        edge.GetBegin()->SetTypeIntersection(TypeIntersection::BEGINVERTEXINT);
        return TypeIntersection::BEGINVERTEXINT;
    }

    // se non si verifica nessuno di questi casi, allora  è una NON INTERSEZIONE

    else
        return TypeIntersection::NOTINT;
}

// dopo il taglio del poligono, restituisce la lista contenente tutti i punti del taglio

vector<IPoint*> PolygonCut::GetFinalPointsList()
{

    for(unsigned int i = 0; i < __segments.size(); i++)
    {
        __finalPointsList.push_back(__segments[i].GetBegin());
        __finalPointsList[__finalPointsList.size()-1]->SetVertex(__segments[i].GetBegin()->GetVertex());
        __finalPointsList.push_back(__segments[i].GetEnd());
        __finalPointsList[__finalPointsList.size()-1]->SetVertex(__segments[i].GetEnd()->GetVertex());
    }

    sort(__finalPointsList.begin(),__finalPointsList.end(), IPoint::cmp);

    for(std::vector<IPoint*>::iterator iter = __finalPointsList.begin(); iter != __finalPointsList.end() - 1; iter++)
        if(**iter == **(iter + 1))
        {
            __finalPointsList.erase(iter);
            iter--;
        }

    return __finalPointsList;
}

// dopo il taglio, restituisce la lista di poligoni ottenuti

vector<vector<unsigned int> > PolygonCut::GetPolygonsList()
{
    vector<vector<unsigned int>> vertex_polygons_list;
    vector<unsigned int> vertex_one_polygon;
    for(unsigned int i=0; i<__polygonToCut.size(); i++)
    {
        vertex_one_polygon.clear();
        for(unsigned j=0; j<__polygonToCut[i]->GetPointsList().size();j++)
            vertex_one_polygon.push_back(__polygonToCut[i]->GetPointsList()[j]->GetVertex());
        vertex_polygons_list.push_back(vertex_one_polygon);
    }
    return vertex_polygons_list;
}

// il metodo crea il poligono e lo manda al metodo che lo taglierà

void GeometryLibrary::PolygonCut::Cut(vector<Interfaces::IPoint*>& points_list, vector<unsigned int>& vertices_list, vector<Interfaces::IPoint*>& segment_list)
{
    __finalPointsList.clear();
    __polygonToCut.clear();
    __segments.clear();

    GenericPolygonFactory polyfac;

    __polygonToCut.push_back(polyfac.CreatePolygon(points_list,vertices_list,true));
    if((segment_list.size()%2) == 1)
        throw runtime_error("Non è possibile identificare i segmenti per il taglio.");
    for(unsigned int i=0; i<0.5*(segment_list.size()); i++)
        __segments.push_back(Segment(segment_list[2*i],segment_list[2*i+1]));
    this->ComputeCut();

}

// il metodo manda il poligono (già creato) al metodo che lo taglierà

void PolygonCut::Cut1(IPolygon* polygon, vector<IPoint*> segment_list)
{
    __finalPointsList.clear();
    __polygonToCut.clear();
    __segments.clear();

    __polygonToCut.push_back(polygon);
    if((segment_list.size()%2) == 1)
        throw runtime_error("Non è possibile identificare i segmenti per il taglio.");
    for(unsigned int i=0; i<0.5*(segment_list.size()); i++)
        __segments.push_back(Segment(segment_list[2*i],segment_list[2*i+1]));
    this->ComputeCut();

}

// una volta compiuto il taglio, ricostruisce i poligoni uno dopo l'altro ricorsivamente

void GeometryLibrary::PolygonCut::PolygonsConstruction(vector<Interfaces::IPoint*>& points_list,Interfaces::Segment segment, Interfaces::IPoint* initialPoint, unsigned int index)
{
    vector<IPoint*> polygon_points_list;
    vector<unsigned int> polygon_vertices_list;

    // questo iteratore è utile per sapere quante volte è già stata fatta partire la funzione di ricostruzinoe

    __iter++;

    if(__iter>1 || __foundPointsList[0]->GetTypeIntersection() == TypeIntersection::NOTINT)
    {
        // in qualsiasi caso, inseriamo il primo punto della lista nella lista (che chiameremo L) dei punti del poligono
        // che stiamo costruendo.
        // Nel momento in cui inseriamo un punto in L, lo togliamo dalla lista generale (LG) (a meno che non sia un'intersezione:
        // in tal caso bisogna fare attenzione)

    polygon_points_list.push_back(points_list[0]);
    points_list.erase(points_list.begin());
    }

    // scorriamo la lista generale di punti finchè non si svuota

    while (points_list.empty() != true) {

        // in caso di una non intersezione: inseriamo in L e togliamo da LG

        if (points_list[0]->GetTypeIntersection() == TypeIntersection::NOTINT) {
            polygon_points_list.push_back(points_list[0]);
            points_list.erase(points_list.begin());
        }
        else {

            // altrimenti inseriamo soltanto in L

            polygon_points_list.push_back(points_list[0]);
            points_list[0]->SetTypeIntersection(TypeIntersection::NOTINT);

            if (__iter == 1)
            {
                // CASO PARTICOLARE: alla prima chiamata, alla prima intersezione, richiamiamo subito la funzione

                this->PolygonsConstruction(points_list,segment, points_list[0], index);

                // finita la costruzione, se ci sono ancora punti in LG, inseriamo il primo in L e lo togliamo da LG

                if(points_list.size()>0)
                {
                    polygon_points_list.push_back(points_list[0]);
                    points_list.erase(points_list.begin());
                }
            }
            else
            {
                // se invece non siamo alla prima iterazione, controlliamo se il segmento dato dal punto iniziale del poligono e
                // dal primo della lista LG è contenuto nel poligono.

                if(this->CheckInside(initialPoint,points_list[0], index) == false)
                {
                    // se non è contenuto, allora non dobbiamo chiudere il poligono, ma iniziare la costruzione del successivo

                    this->PolygonsConstruction(points_list, segment, points_list[0], index);
                    polygon_points_list.push_back(points_list[0]);
                    points_list.erase(points_list.begin());
                }
                else
                {
                    // se il segmento è contenuto, bisogna orientare in maniera corretta il segmento che chiude il poligono;
                    // inoltre aggiungiamo alla lista di punti (se è il caso di farlo) i punti s1 e s2 del segmento tagliante
                    // ricevuto in input

                    double t_finale, t_iniziale;
                    IPoint *punto_iniziale, *punto_finale;

                    t_iniziale = (initialPoint->GetX() - segment.GetBegin()->GetX())/(segment.GetDirection()[0]);
                    t_finale = (polygon_points_list[polygon_points_list.size() - 1]->GetX() - segment.GetBegin()->GetX())/(segment.GetDirection()[0]);
                    punto_iniziale = segment.GetBegin();
                    punto_finale = segment.GetEnd();
                    if(t_finale < t_iniziale + TOLL)
                    {
                        double tmp_t = t_finale;
                        t_finale = t_iniziale;
                        t_iniziale = tmp_t;

                        if(t_finale>1 + TOLL && t_iniziale<-TOLL)
                        {
                            IPoint* tmp_p = punto_finale;
                            punto_finale = punto_iniziale;
                            punto_iniziale = tmp_p;
                        }

                        // infine inseriamo gli ultimi due punti nella lista L

                    }
                    if(t_finale>1+TOLL && t_iniziale<1 - TOLL)
                    {
                        polygon_points_list.push_back(punto_finale);
                        points_list.insert(points_list.begin(),punto_finale);
                        points_list[1]->SetTypeIntersection(TypeIntersection::NOTINT);
                    }

                    if(t_finale>TOLL && t_iniziale<-TOLL)
                    {
                        polygon_points_list.push_back(punto_iniziale);
                        points_list.insert(points_list.begin(),punto_iniziale);
                        points_list[1]->SetTypeIntersection(TypeIntersection::NOTINT);
                    }
                    break;
                }
            }
        }

    }

    // concluso il procedimento, decrementiamo iter
    __iter--;

    // inseriamo poi il poligono ottenuto nella lista di poligoni costruiti

    for(unsigned int i = 0; i<polygon_points_list.size();i++)
        polygon_vertices_list.push_back(polygon_points_list[i]->GetVertex());
    GenericPolygonFactory polyfac;
    __polygonsList.push_back(polyfac.CreatePolygon(polygon_points_list,polygon_vertices_list,false));

}

// vero e proprio taglio del poligono

void PolygonCut::ComputeCut()
{

  for(unsigned int k =0; k < __segments.size(); k++)
  {
    for(unsigned int y=0; y<__polygonToCut.size(); y++)
    {
        // scorriamo la lista di segmenti che tagliano e la lista dei poligoni da tagliare

        __intersectionSegments.clear();
        __foundPointsList.clear();

        // definiamo la lista di intersezioni e la lista di segmenti del poligono interessato dal taglio

        vector<IPoint*> intersections_list;
        vector<Segment> segments_list = __polygonToCut[y]->GetSegmentsList();

        // ricaviamo il "vertice massimo" che servirà per l'assegnazione del vertice ai punti di intersezione

        unsigned int l = 0;
        for(unsigned int i=0; i <  __polygonToCut[y]->GetPointsList().size(); i++)
            if( l < __polygonToCut[y]->GetPointsList()[i]->GetVertex())
                    l = __polygonToCut[y]->GetPointsList()[i]->GetVertex();
        l++;

        // calcoliamo, segmento dopo segmento, le intersezioni e assegniamo ad esse il vertice corrispondente

        for (unsigned int i = 0; i < segments_list.size(); i++)
        {
            __foundPointsList.push_back(segments_list[i].GetBegin());
            TypeIntersection type  = IntersectionPoint::ComputeIntersection(segments_list[i], __segments[k]);

            if (type == TypeIntersection::PROPERINT) // caso 1: INTERSEZIONE PROPRIA
            {
                __foundPointsList.push_back(IntersectionPoint::GetFoundPoint());
                __foundPointsList[__foundPointsList.size()-1]->SetVertex(l);
                intersections_list.push_back(IntersectionPoint::GetFoundPoint());
                l++;
                __intersectionSegments.push_back(segments_list[i]);
            }

            if (type == TypeIntersection::BEGINVERTEXINT) // caso 2: INTERSEZIONE AL VERTICE
            {
                __foundPointsList[__foundPointsList.size() - 1]->SetTypeIntersection(TypeIntersection::BEGINVERTEXINT);
                intersections_list.push_back(segments_list[i].GetBegin());
                __intersectionSegments.push_back(segments_list[i]);
            }
        }

        // passiamo a valutare le  intersezioni al vertice

        unsigned int h = 0;
        for (unsigned int j = 0; j < __foundPointsList.size(); j++)
        {
            // per prima cosa controlliamo se è un'intersezione propria

            if (__foundPointsList[j]->GetTypeIntersection() == TypeIntersection::PROPERINT)
                h += 1;

            else if (__foundPointsList[j]->GetTypeIntersection() == TypeIntersection::BEGINVERTEXINT || __foundPointsList[j]->GetTypeIntersection() == TypeIntersection::ENDVERTEXINT)
            {
                // nel caso in cui l'intersezione al vertice sia l'unica intersezione, allora la considereremo una non intersezione

                if (intersections_list.size() == 1)
                    __foundPointsList[j]->SetTypeIntersection(TypeIntersection::NOTINT);

                else

                    // nel momento in cui un'intersezione al vertice non è l'unica intersezione, allora possiamo considerarla
                    // un'intersezione propria e trattarla come tale, se il segmento formato da essa e l'intersezione immediatamente
                    // precedente o successiva è contenuto nel poligono
                {
                    if (h < (intersections_list.size() -1) && CheckInside(__foundPointsList[j], intersections_list[h+1], y) == true)
                        __foundPointsList[j]->SetTypeIntersection(TypeIntersection::PROPERINT);
                    else if (h > 0 && CheckInside(__foundPointsList[j], intersections_list[h-1], y) == true)
                        __foundPointsList[j]->SetTypeIntersection(TypeIntersection::PROPERINT);
                    else
                        __foundPointsList[j]->SetTypeIntersection(TypeIntersection::NOTINT);
                }

                h++;
            }
        }

        // assegniamo i vertici ai punti che definiscono il segmento che taglia (se non sono già intersezioni => in tal caso
        // l'abbiamo già assegnato)

        if (__segments[k].GetBegin()->GetTypeIntersection() == TypeIntersection::NOTINT)
            __segments[k].GetBegin()->SetVertex(l);
        if (__segments[k].GetEnd()->GetTypeIntersection() == TypeIntersection::NOTINT)
            __segments[k].GetEnd()->SetVertex(l+1);

        // definiamo e riempiamo una lista di punti da mandare alla funzione che costruisce i poligoni

        vector<IPoint*> sending_list;
        PointFactory pointfac;
        for (unsigned int j = 0; j < __foundPointsList.size(); j++)
        {
            sending_list.push_back(pointfac.CreatePoint( __foundPointsList[j]->GetX(),  __foundPointsList[j]->GetY()));
            sending_list[j]->SetVertex(__foundPointsList[j]->GetVertex());
            sending_list[j]->SetTypeIntersection(__foundPointsList[j]->GetTypeIntersection());
        }
        PolygonsConstruction(sending_list,__segments[k], __foundPointsList[0], y);
        for(unsigned int i=0; i < sending_list.size(); i++)
            delete sending_list[i];
    }

    // a questo punto, dopo taglio e ricostruzione, vengono resettate le liste e viene aggiornata la lista finalPointsList

    for(unsigned int i=0; i < __polygonToCut.size(); i++)
        __polygonToCut[i]->ResetPointsIntersectionType();
    __polygonToCut.clear();
    for(unsigned i = 0; i < __polygonsList.size();i++)
    {
        __polygonsList[i]->ResetPointsIntersectionType();
        __polygonToCut.push_back(__polygonsList[i]);
    }
    __finalPointsList.insert(__finalPointsList.end(), __foundPointsList.begin(), __foundPointsList.end());
   __polygonsList.clear();
  }
}

// il metodo verifica se un segmento è dentro un poligono oppure no

bool GeometryLibrary::PolygonCut::CheckInside(Interfaces::IPoint*& p1, Interfaces::IPoint*& p2, unsigned int index)
{
    PointFactory pointfac;
    IPoint* p3 = pointfac.CreatePoint(p1->GetX(), p1->GetY());
    IPoint* p4 = pointfac.CreatePoint(p2->GetX(), p2->GetY());

    bool flag = false;

    // creazione del segmento, dati i due punti

    Segment segment(p3,p4);

    // calcolo del punto medio del segmento

    IPoint* midpoint = pointfac.CreatePoint(0.5*(p3->GetX()+ p4->GetX()),0.5*(p3->GetY() +p4->GetY()));

    for(unsigned int i = 0; i<__intersectionSegments.size();i++)

        // verifichiamo se il segmento ha intersezioni proprie con i segmenti del poligono:
        // dato che consideriamo il segmento in questione come segmento vero e proprio (e non come retta), allora
        // se esistono intersezioni proprie, non è interno

        if( IntersectionPoint::ComputeIntersection(segment,__intersectionSegments[i]) == TypeIntersection::PROPERINT)
        {
            delete p3, delete p4, delete midpoint;
            return flag;

        }

    // se invece non ci sono int. proprie:

    flag = true;
    for(unsigned int i = 0; i <__polygonToCut[index]->GetSegmentsList().size()-1; i++)
    {
        // consideriamo una coppia di segmenti del poligono adiacenti e salviamo i relativi tre punti

        IPoint* p1_ = pointfac.CreatePoint(__polygonToCut[index]->GetSegmentsList()[i].GetBegin()->GetX(), __polygonToCut[index]->GetSegmentsList()[i].GetBegin()->GetY());
        IPoint* p2_ = pointfac.CreatePoint(__polygonToCut[index]->GetSegmentsList()[i].GetEnd()->GetX(), __polygonToCut[index]->GetSegmentsList()[i].GetEnd()->GetY());
        IPoint* p3_ = pointfac.CreatePoint(__polygonToCut[index]->GetSegmentsList()[i+1].GetEnd()->GetX(), __polygonToCut[index]->GetSegmentsList()[i+1].GetEnd()->GetY());
        if((p1_->GetX() - p2_->GetX())*(p3_->GetY() - p2_->GetY()) - (p3_->GetX() - p2_->GetX())*(p1_->GetY()-p2_->GetY()) > 1e-7)
        {
            // consideriamo il triangolo generato da questi tre punti

            vector<IPoint*> list = {p3_, p2_, p1_};
            vector<unsigned int> vert = {0,1,2};
            ConvexPolygonFactory polyfac;
            IPolygon* poly = polyfac.CreatePolygon(list,vert,false);
            if(poly->PointInPolygon(*midpoint) == true)
            {
                flag = false;
                delete p3_, delete p2_, delete p1_;
                break;
            }
          delete p3_, delete p2_, delete p1_;
        }
    }

    delete p3, delete p4, delete midpoint;

    return flag;
}

// creazione della mesh, ossia della struttura che contiene tutti i poligoni


vector<Interfaces::IPolygon*> GeometryLibrary::Mesh::CreateMesh(Interfaces::IPolygon* domain, GeometryObject::ReferenceElement &rf)
{
    // chiameremo rf l'elemento di riferimento
    if(!__polygonInMesh.empty())
        for(unsigned int i=0; i < __polygonInMesh.size(); i++)
            __polygonInMesh[i]->~IPolygon();
    __polygonInMesh.clear();

    vector<IPoint*> domain_Bb = domain->GetBoundingBox();

    // traslazione del primo rf in corrispondenza del vertice in basso a sinistra della bounding box del dominio (che chiameremo Bb)

    rf.TraslateReferenceElement(domain_Bb[0]->GetX() - rf.GetBoundingBox()[0]->GetX(), domain_Bb[0]->GetY() - rf.GetBoundingBox()[0]->GetY());

    // calcolo quanti rf sono contenuti nella Bb, sia in altezza che in larghezza

    unsigned int numb = abs(int((domain_Bb[1]->GetX() - domain_Bb[0]->GetX())/rf.GetB())) + 1;
    unsigned int numh = abs(int((domain_Bb[3]->GetY() - domain_Bb[0]->GetY())/rf.GetH())) + 1;

    GenericPolygonFactory polyfac;

    // creazione della lista di segmenti del dominio (saranno i segmenti che tagliano)

    PolygonCut polycut;

    for(unsigned int i=0; i<numh; i++)
    {
        // traslazione del rf in altezza

        if(i!=0)
            rf.TraslateReferenceElement(0,rf.GetH());
        for(unsigned int j = 0; j <numb; j++)
        {
            // traslazione del rf in larghezza

            if(j!=0)
                rf.TraslateReferenceElement(rf.GetB(),0);

            bool flag = false;

            // verifichiamo se rf sia contenuto interamente nel dominio (in particolare controlliamo i punti
            // della bounding box)

            for(unsigned int k = 0; k < rf.GetPolygonList()[rf.GetPolygonList().size() - 1]->GetBoundingBox().size(); k++)
            {
                flag = domain->PointInPolygon(*rf.GetPolygonList()[rf.GetPolygonList().size() - 1]->GetBoundingBox()[k]);
                if (flag == false)
                    break;
            }

            if (flag == true)

                // nel caso in cui sia interamente contenuto, allora aggiungiamo tutti i poligoni di rf nella mesh
            {
               for(unsigned int k = 0; k < rf.GetPolygonList().size(); k++)
               {
                   vector<unsigned int> vertex_list;
                   for(unsigned int l = 0; l < rf.GetPolygonList()[k]->GetPointsList().size(); l++)
                       vertex_list.push_back(rf.GetPolygonList()[k]->GetPointsList()[l]->GetVertex());
                   __polygonInMesh.push_back(polyfac.CreatePolygon(rf.GetPolygonList()[k]->GetPointsList(),vertex_list,false));
               }
            }

            // quando invece rf non è totalmente contenuto, allora bisogna calcolare le intersezioni e determinare
            // quali saranno i poligoni appartenenti alla mesh (se ci saranno)

            else
            {
                vector<IPoint*> intlist;
                intlist.clear();
                vector <unsigned int> vl = {0,1,2,3};

                // consideriamo la bounding box (come poligono) dell'ultimo poligono di rf, ossia quello principale

                vector <IPoint*> bb = rf.GetPolygonList()[rf.GetPolygonList().size()-1]->GetBoundingBox();
                ConvexPolygonFactory polyfac;
                IPolygon* _Bb = polyfac.CreatePolygon(bb, vl, false);

                // calcoliamo tutte le intersezioni tra i segmenti del dominio e i segmenti della bounding box di poco sopra

                for(unsigned k = 0; k < domain->GetSegmentsList().size(); k++)
                    for(unsigned int h = 0; h < _Bb->GetSegmentsList().size(); h++)

                        // nel caso di intersezioni proprie, salviamo i segmenti del dominio interessati da esse

                        if(IntersectionPoint::ComputeIntersection(_Bb->GetSegmentsList()[h], domain->GetSegmentsList()[k]) == TypeIntersection::PROPERINT)
                        {
                            intlist.push_back(domain->GetSegmentsList()[k].GetBegin());
                            intlist.push_back(domain->GetSegmentsList()[k].GetEnd());
                            break;
                        }

                // per ogni poligono di rf, calcoliamo le sue intersezioni con i lati precedentemente salvati

                if(intlist.size() != 0)
                {
                    for (unsigned int k = 0; k < rf.GetPolygonList().size(); k++)
                    {
                        polycut.Cut1(rf.GetPolygonList()[k], intlist);                // taglio del poligono

                        for(unsigned int h = 0; h < polycut.GetPolygonsList1().size(); h++)
                        {
                            bool flag = false;
                            vector<IPoint*> list = polycut.GetPolygonsList1()[h]->GetPointsList();
                            for(std::vector<IPoint*>::iterator iter = list.begin(); iter != list.end(); iter++)
                            {
                                // verifichiamo che tutti i punti del poligono tagliato siano interni al dominio

                                flag = domain->PointInPolygon(*(*iter));
                                if(flag == false)
                                    break;
                            }

                            // se il controllo va a buon fine, allora possiamo inserire il poligono tagliato nella mesh

                            if(flag == true)
                            {
                                vector<unsigned int> vertex_list;
                                for(unsigned int l = 0; l < polycut.GetPolygonsList1()[h]->GetPointsList().size(); l++)
                                    vertex_list.push_back(polycut.GetPolygonsList1()[h]->GetPointsList()[l]->GetVertex());
                                __polygonInMesh.push_back(polyfac.CreatePolygon(polycut.GetPolygonsList1()[h]->GetPointsList(),vertex_list,false));
                            }

                        }
                    }
                }
            }
        }
        rf.TraslateReferenceElement(-((numb-1)*(rf.GetB())), 0);
    }

    return __polygonInMesh;

}

// stampa della mesh, ossia creazione del file .m per ottenere, attraverso MaatLab, la rappresentazione grafica

void Mesh::PrintMesh()
{
    // creazione del file

    ofstream file("plotCellFound.m", ios::out);

    unsigned int cnt = 1;

    if(file.is_open())
    {
        // scrittura del file

        for(std::vector<IPolygon*>::iterator iter = __polygonInMesh.begin(); iter != __polygonInMesh.end(); iter++)
        {
           file << "nodes" <<cnt<< "= [" ;
           vector<IPoint*> list = (*iter)->GetPointsList();

           // inserimento dei punti

           for(unsigned int i = 0; i < list.size()-1; i++)
               file << list[i]->GetX() << "," << list[i]->GetY()<< ";" <<endl;
           file << list[list.size()-1]->GetX() << "," << list[list.size()-1]->GetY() << endl;
           file << "];" << endl;

           // creazione dei poligoni

           file<< "polygon"<<cnt<<"=polyshape(nodes"<<cnt<<"(:,1),nodes"<<cnt<<"(:,2));"<<endl;

           // stampa dei poligoni

           file<<"plot(polygon"<<cnt<<")\n hold on"<<endl;
           cnt++;
    }

    file.close();
    }
}
}

