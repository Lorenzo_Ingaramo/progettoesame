#include "GeometryObject.hpp"
#include "Interfaces.hpp"

using namespace Interfaces;

namespace GeometryObject {

// costruttore del poligono data la lista di punti, di vertici e il valore booleano

GeometryObject::Polygon::Polygon(vector<Interfaces::IPoint*>& points, vector<unsigned int>& vertex, bool sort_vertex)
{
    // associazione dei vertici ai punti

    for(unsigned int i=0; i<points.size(); i++)
        points[i]->SetVertex(vertex[i]);

    // se il bool sort_vertex è false, allora non è il caso di ordinare i punti in quanto la lista è già ordinata;
    // se invece sor_vertex = true, allora vanno ordinati i vertici:

    if( sort_vertex == true)
    {
        IPoint* tmp;
        for(unsigned int i=0; i < points.size() - 1; i++)
        {
            for(unsigned int j = i; j < points.size(); j++)
                if(points[i]->GetVertex() > points[j]->GetVertex())
                {
                    tmp = points[i];
                    points[i] = points[j];
                    points[j] = tmp;
                }
        }
    }

    // realizzazione della lista di punti

    for(unsigned int i=0; i<points.size();i++)
    {
        PointFactory pointfac;
        __pointsList.push_back(pointfac.CreatePoint(points[i]->GetX(), points[i]->GetY()));
        __pointsList[i]->SetVertex(points[i]->GetVertex());
    }

    // creazione dei segmenti, una volta ottenuti i punti in ordine

    for(unsigned int i = 0; i < __pointsList.size() - 1; i++)
        __segmentsList.push_back(Segment(__pointsList[i],__pointsList[i+1]));
    __segmentsList.push_back(Segment( __pointsList[__pointsList.size()-1],__pointsList[0]));

}

// costruttore dato un poligono

Polygon::Polygon(const Polygon& poly)
{
    // realizzazione della lista di punti

    for(unsigned int i=0; i < poly.GetPointsList().size(); i++)
    {
        PointFactory pointfac;
        __pointsList.push_back(pointfac.CreatePoint(poly.GetPointsList()[i]->GetX(), poly.GetPointsList()[i]->GetY()));
        __pointsList[i]->SetVertex(poly.GetPointsList()[i]->GetVertex());
    }

    // realizzazione della lista di segmenti

    for(unsigned int i = 0; i < __pointsList.size() - 1; i++)
        __segmentsList.push_back(Segment(__pointsList[i],__pointsList[i+1]));
    __segmentsList.push_back(Segment( __pointsList[__pointsList.size()-1],__pointsList[0]));
}

// procedimento analogo a prima, ma per i poligoni convessi

ConvexPolygon::ConvexPolygon(vector<IPoint *> &points, vector<unsigned int> &vertex, bool sort_vertex)
{
    for(unsigned int i=0; i<points.size(); i++)
        points[i]->SetVertex(vertex[i]);

    if( sort_vertex == true)
    {
        IPoint* tmp;
        for(unsigned int i=0; i < points.size() - 1; i++)
        {
            for(unsigned int j = i; j < points.size(); j++)
                if(points[i]->GetVertex() > points[j]->GetVertex())
                {
                    tmp = points[i];
                    points[i] = points[j];
                    points[j] = tmp;
                }
        }
    }

    for(unsigned int i=0; i<points.size();i++)
    {
        PointFactory pointfac;
        __pointsList.push_back(pointfac.CreatePoint(points[i]->GetX(), points[i]->GetY()));
        __pointsList[i]->SetVertex(points[i]->GetVertex());
    }

    for(unsigned int i = 0; i < __pointsList.size() - 1; i++)
        __segmentsList.push_back(Segment(__pointsList[i],__pointsList[i+1]));
    __segmentsList.push_back(Segment( __pointsList[__pointsList.size()-1],__pointsList[0]));
}

// procedimento analogo a prima, ma per i poligoni convessi

ConvexPolygon::ConvexPolygon(const Polygon &poly)
{
    for(unsigned int i=0; i < poly.GetPointsList().size(); i++)
    {
        PointFactory pointfac;
        __pointsList.push_back(pointfac.CreatePoint(poly.GetPointsList()[i]->GetX(), poly.GetPointsList()[i]->GetY()));
        __pointsList[i]->SetVertex(poly.GetPointsList()[i]->GetVertex());
    }
    for(unsigned int i = 0; i < __pointsList.size() - 1; i++)
        __segmentsList.push_back(Segment(__pointsList[i],__pointsList[i+1]));
    __segmentsList.push_back(Segment( __pointsList[__pointsList.size()-1],__pointsList[0]));
}

// il metodo verifica, per poligoni convessi, se un punto si trova all'interno del poligono

bool ConvexPolygon::PointInPolygon(const IPoint &p) const
{
    // utilizziamo questo criterio: verifichiamo che, per qualsiasi tipologia di segmento,
    // il punto sia alla sinistra di esso immaginando di percorrere il segmento dal punto iniziale al punto finale

    bool flag = false;
    for(unsigned int i=0; i < __segmentsList.size();i++)
    {
        flag = false;

        // Prima possibilità: segmento verticale

        if(abs(__segmentsList[i].GetDirection()[0]) < TOLL)
        {
          if(__segmentsList[i].GetDirection()[1]< -TOLL)  // segmento orientato verso il basso
            flag = ( p.GetX() >= __segmentsList[i].GetBegin()->GetX() - TOLL);

          if(__segmentsList[i].GetDirection()[1]> TOLL)  // segmento orientato verso l'alto
            flag = ( p.GetX()  <=  __segmentsList[i].GetBegin()->GetX() + TOLL);
        }

        // Seconda possibilità: segmento orizzontale

        else
            if(abs(__segmentsList[i].GetDirection()[1]) <TOLL)
            {
                if(__segmentsList[i].GetDirection()[0] < -TOLL) // segmento orientato verso sinistra
                    flag = ( p.GetY() <= __segmentsList[i].GetBegin()->GetY() + TOLL);

                if(__segmentsList[i].GetDirection()[0] > TOLL)  // segmento orientato verso destra
                    flag = ( p.GetY() >= __segmentsList[i].GetBegin()->GetY() - TOLL);
            }  

                // Terza possibilità: segmento generico

        else
                {
                    double rapp = (__segmentsList[i].GetDirection()[1])/(__segmentsList[i].GetDirection()[0]);
                    if(__segmentsList[i].GetDirection()[0] > -TOLL)  // segmento orientato verso destra
                        flag = (p.GetY() >= __segmentsList[i].GetBegin()->GetY() + rapp* (p.GetX() - __segmentsList[i].GetBegin()->GetX()) -TOLL);

                    if(__segmentsList[i].GetDirection()[0] < TOLL)  // segmento orientato verso sinistra
                         flag = (p.GetY() <= __segmentsList[i].GetBegin()->GetY() + rapp * (p.GetX() - __segmentsList[i].GetBegin()->GetX()) + TOLL);
                }

        // uno e uno solo dei tre if è soddisfatto; dopodichè all'interno di ogni caso, se non viene
        // verificata nessuna delle due possibilità, allora il flag rimane false: in tal caso il punto NON è interno al poligono

        if( flag == false)
            break;
    }
    return flag;
}

// il metodo crea l'elemento di riferimento: poligono + bounding box

GeometryObject::ReferenceElement &GeometryObject::ReferenceElement::CreateReferenceElement(Interfaces::IPolygon*& polygon, vector<Interfaces::IPoint*> boundingbox)
{

    // riempimento della lista __boundingBox

    for(unsigned int i = 0; i < boundingbox.size(); i++)
        __boundingBox.push_back(boundingbox[i]);

    // inizializzazione di 4 lista di punti ausiliarie

    vector<IPoint*> up;
    vector<IPoint*> down;
    vector<IPoint*> left;
    vector<IPoint*> right;

    vector<IPoint*> Pointlist = polygon->GetPointsList();
    PointFactory pointfac;

    // calcolo di base e altezza della bounding box

    double l = __boundingBox[2]->GetY() - __boundingBox[1]->GetY();
    double b = __boundingBox[1]->GetX() - __boundingBox[0]->GetX();

    __b = b;
    __h = l;

    // inizia ora la costruzione di un nuovo concetto di bounding box: sarà infatti una lista composta, ordinatamente,
    // dai 4 vertici della vecchia bounding box, dalle intersezioni tra lati del poligono e bounding box e dalle proiezioni di questi
    // ultimi punti sul lato opposto della bounding box. Infatti quando affiancheremo gli elementi di riferimento, questi punti
    // coincideranno con le intersezioni vere e proprie dei poligoni adiacenti

    for(unsigned int i=0; i< Pointlist.size();i++)
    {
        bool flag = false;

        // Primo lato: base inferiore del rettangolo

        if(abs(Pointlist[i]->GetY() - __boundingBox[0]->GetY()) < TOLL)
        {
            flag = false;
            Pointlist[i]->SetBbType(onBoundingBox::INBb);

            for(unsigned int j=0; j < down.size(); j++)

                if( abs(down[j]->GetX() - Pointlist[i]->GetX()) < TOLL)
                {
                    flag =true;
                    break;
                }

            // inserimento del punto nella lista di punti down

            if (flag==false)
                down.push_back(pointfac.CreatePoint(Pointlist[i]->GetX(),Pointlist[i]->GetY()));

            flag = false;
            for(unsigned int j=0; j < up.size(); j++)

                // controlliamo che in corrispondenza della proiezione non sia già presente un punto

                if( abs(up[j]->GetX() - Pointlist[i]->GetX())< TOLL)
                {
                    flag =true;
                    break;
                }

            // inserimento della proiezione del punto nella lista up

            if (flag==false)
                up.push_back(pointfac.CreatePoint(Pointlist[i]->GetX(),Pointlist[i]->GetY() + l));
        }

        // Secondo lato: altezza destra del rettangolo

        if( abs(Pointlist[i]->GetX() - boundingbox[1]->GetX()) < TOLL)
        {
            flag = false;
            Pointlist[i]->SetBbType(onBoundingBox::INBb);

            for(unsigned int j=0; j < right.size(); j++)

                if( abs(right[j]->GetY() - Pointlist[i]->GetY())< TOLL)
                {
                    flag =true;
                    break;
                }

            // inserimento del punto nella lista di punti right

            if (flag==false)
                right.push_back(pointfac.CreatePoint(Pointlist[i]->GetX(),Pointlist[i]->GetY()));

            flag = false;
            for(unsigned int j=0; j < left.size(); j++)

                // controlliamo che in corrispondenza della proiezione non sia già presente un punto

                if( abs(left[j]->GetY() - Pointlist[i]->GetY())< TOLL)
                {
                    flag =true;
                    break;
                }

            // inserimento della proiezione del punto nella lista left

            if (flag==false)
                left.push_back(pointfac.CreatePoint(Pointlist[i]->GetX()-b,Pointlist[i]->GetY()));
        }

        // Terzo lato: base superiore del rettangolo

        if(abs(Pointlist[i]->GetY() - boundingbox[2]->GetY()) <TOLL)
        {
            flag = false;
            Pointlist[i]->SetBbType(onBoundingBox::INBb);

            for(unsigned int j=0; j < up.size(); j++)

                if( abs(up[j]->GetX() - Pointlist[i]->GetX())< TOLL)
                {
                    flag =true;
                    break;
                }

            // inserimento del punto nella lista di punti up

            if (flag==false)
                up.push_back(pointfac.CreatePoint(Pointlist[i]->GetX(),Pointlist[i]->GetY()));

            flag = false;
            for(unsigned int j=0; j < down.size(); j++)

                // controlliamo che in corrispondenza della proiezione non sia già presente un punto

                if( abs(down[j]->GetX() - Pointlist[i]->GetX())< TOLL)
                {
                    flag =true;
                    break;
                }

            // inserimento della proiezione del punto nella lista down

            if (flag==false)
                down.push_back(pointfac.CreatePoint(Pointlist[i]->GetX(),Pointlist[i]->GetY()-l));
        }

         // Quarto lato: altezza sinistra del rettangolo

        if(abs(Pointlist[i]->GetX() - boundingbox[0]->GetX())< TOLL)
        {
            flag = false;
            Pointlist[i]->SetBbType(onBoundingBox::INBb);

            for(unsigned int j=0; j < left.size(); j++)

                if( abs(left[j]->GetY() - Pointlist[i]->GetY())< TOLL)
                {
                    flag =true;
                    break;
                }

            // inserimento del punto nella lista di punti left

            if (flag==false)
                left.push_back(pointfac.CreatePoint(Pointlist[i]->GetX(),Pointlist[i]->GetY()));

            flag = false;
            for(unsigned int j=0; j < right.size(); j++)

                // controlliamo che in corrispondenza della proiezione non sia già presente un punto

                if( abs(right[j]->GetY() - Pointlist[i]->GetY())< TOLL)
                {
                    flag =true;
                    break;
                }

            // inserimento della proiezione del punto nella lista right

            if (flag==false)
                right.push_back(pointfac.CreatePoint(Pointlist[i]->GetX()+b,Pointlist[i]->GetY()));
        }
    }

    // ordinamento di tutte e 4 le liste di appoggio

    sort(down.begin(), down.end(), IPoint::cmp);
    sort(up.begin(), up.end(),IPoint::cmp);
    sort(left.begin(), left.end(),IPoint::cmp);
    sort(right.begin(), right.end(),IPoint::cmp);

    // creazione di un iteratore per concatenare correttamente le 4 liste (down -> right -> up -> left)

    std::vector<IPoint*>::iterator iter = __boundingBox.begin();
    iter++;

    for(std::vector<IPoint*>::iterator iterlist = down.begin(); iterlist != down.end(); iterlist++)
    {
        iter =__boundingBox.insert(iter, *iterlist);
        iter++;
    }

    iter++;
    for(std::vector<IPoint*>::iterator iterlist = right.begin(); iterlist != right.end(); iterlist++)
    {
        iter =__boundingBox.insert(iter, *iterlist);
        iter++;
    }

    iter++;
    for(std::vector<IPoint*>::reverse_iterator iterlist = up.rbegin(); iterlist != up.rend(); iterlist ++)
    {
        iter =__boundingBox.insert(iter, *iterlist);
        iter++;
    }

    iter++;
    for(std::vector<IPoint*>::reverse_iterator iterlist =left.rbegin(); iterlist != left.rend() ; iterlist++)
    {
        iter =__boundingBox.insert(iter, *iterlist);
        iter++;
    }

    // creazione dei poligoni dell'elemento di riferimento
    std::vector<IPoint*>::iterator begin = __boundingBox.begin();
    std::vector<IPoint*>::iterator end;

    // creazione di liste d'appoggio

    vector<IPoint*> newpolygon;
    vector<unsigned int> newvertex;
    vector<IPoint*> polygon_points;
    vector<unsigned int> polygon_vertex;

    vector<IPoint*> newpolygon1;
    vector<unsigned int> newvertex1;

    GenericPolygonFactory polyfac;

    for(unsigned int i=0; i < Pointlist.size() - 1;i++)
    {
        unsigned int m = __boundingBox.size() + Pointlist.size();

        newpolygon1.push_back(Pointlist[i]);

        // cerchiamo innanzitutto un punto (A) comune a poligono principale (che chiameremo POL) e bounding box


        if(Pointlist[i]->GetBbType() == onBoundingBox::INBb)
        {
            // facciamo coincidere l'iteratore begin con A

           while(*(*begin) != *Pointlist[i])
               begin++;

           // controlliamo che il punto successivo ad A nel poligono POL non coincida (a meno della tolleranza) con A

           if(abs(Pointlist[i+1]->GetX() - (*begin)->GetX()) > TOLL && abs(Pointlist[i+1]->GetY() - (*begin)->GetY()) > TOLL )
           {
               for(unsigned j=i+1; j<Pointlist.size(); j++)
               {
                   // cerchiamo, dopo A, il punto successivo comune a bounding box e POL (B)

                   if(Pointlist[j]->GetBbType() ==onBoundingBox::INBb)
                   {
                       // facciamo coincidere l'iteratore end con B

                     for(end = begin; end != __boundingBox.end(); end++)
                         if(*(*end) == *Pointlist[j])
                             break;
                     break;
                   }

                   // dati begin ed end, costruiamo il poligono: partiamo da begin e percorriamo in senso antiorario la
                   // bounding box fino ad end; dopodichè percorriamo in senso inverso POL ed otteniamo il poligono.

                   else
                   {
                       newpolygon.push_back(Pointlist[j]);
                       newvertex.push_back(m);
                       m--;
                   }
               }

               // riempimento della lista di poligoni dell'elemento di riferimento

            unsigned int cnt = 1;
               for(std::vector<IPoint*>::iterator it = begin; it != end + 1; it++)
               {
                   newpolygon.push_back(*it);
                   newvertex.push_back(cnt);
                   cnt++;

               }
              __polygonsList.push_back(polyfac.CreatePolygon(newpolygon,newvertex,true));
              newpolygon.clear();
              newvertex.clear();
        }
        else
               //aggiungiamo i punti che stanno su un segmento del poligono principale
           {
               std::vector<IPoint*>::iterator iter = begin;
               //troveremo il punto successivo del poligono sullo stesso lato della Bb
               while(*(*iter) != *Pointlist[i+1])
               {
                   newpolygon1.push_back(*iter);
                   iter++;
               }
           }
        }
    }

    // finita questa procedura, potremmo dover ancora aggiungere un poligono.
    // Infatti, se l'ultimo punto di POL non è nello stesso lato della ounding box del primo punto =>
    // => allora manca un poligono, che viene aggiunto separatamente

    newpolygon1.push_back(Pointlist[Pointlist.size()-1]);

    if(abs(Pointlist[polygon->GetPointsList().size() - 1]->GetX() - Pointlist[0]->GetX()) > TOLL
            && abs(Pointlist[polygon->GetPointsList().size() - 1]->GetY() - Pointlist[0]->GetY()) > TOLL)

    {
        while(*(*begin) != *Pointlist[polygon->GetPointsList().size() - 1])
            begin++;

        unsigned int i =0, cnt = 1;
        for(i = 0; i < __boundingBox.size(); i++)

            if(__boundingBox[i] == *begin)
                break;

        while(*__boundingBox[i%(__boundingBox.size())] != *Pointlist[0])
        {
            newpolygon.push_back(__boundingBox[i%__boundingBox.size()]);
            newvertex.push_back(cnt);
            cnt++;
            i++;
        }

        newpolygon.push_back(Pointlist[0]);
        newvertex.push_back(cnt);

        // aggiornamento della lista di poligoni

        __polygonsList.push_back(polyfac.CreatePolygon(newpolygon,newvertex,true));
    }

    //eliminazione di eventuali punti ripetuti nel poligono principale (nuovo):
    for(std::vector<IPoint*>::iterator iter = newpolygon1.begin(); iter!= newpolygon1.end() - 1;iter++)
        if(**(iter) == **(iter+1))
        {
            newpolygon1.erase(iter);
            iter--;
        }

    for(unsigned int i=0; i < newpolygon1.size(); i++)
        newvertex1.push_back(i);
    __polygonsList.push_back(polyfac.CreatePolygon(newpolygon1,newvertex1,false));


    return *this;
}

// traslazione dell'elemento di riferimento, attraverso la funzione Translate

void GeometryObject::ReferenceElement::TraslateReferenceElement(double h, double b)
{
    for(unsigned int i=0; i< __polygonsList.size(); i++)
        __polygonsList[i]->Translate(h, b);
}


}

