#include "Geometryfunction.hpp"
#include "GeometryObject.hpp"

using namespace GeometryObject;

namespace Interfaces {

// costruttore del segmento

Interfaces::Segment::Segment(Interfaces::IPoint* p1,Interfaces::IPoint* p2)
{
    if(p1 == p2)
        throw runtime_error("Segmento non creato: punti troppo vicini");
    __begin = p1;
    __end = p2;
    __direction[0] = __end->GetX() - __begin->GetX();
    __direction[1] = __end->GetY() - __begin->GetY();

    __normalDirection[0] = __direction[1];
    __normalDirection[1] = -(__direction[0]);
}

// calcolo dell'area del poligono secondo la formula di Gauss che permette di calcolare l'area, noti i soli vertici

double IPolygon::ComputeArea()
{

    double area = 0;
    unsigned int i = 0;
    for(i=0; i < __pointsList.size(); i++ )
      area += (__pointsList[i]->GetX()*__pointsList[(i+1)%__pointsList.size()]->GetY() - __pointsList[i]->GetY()*__pointsList[(i+1)%__pointsList.size()]->GetX());
    return 0.5*area;

}

// traslazione del poligono e della sua bounding box

void IPolygon::Translate(double traslata_x, double traslata_y)
{
    for(unsigned int i=0; i < __pointsList.size();i++)
    {
        __pointsList[i]->SetX(__pointsList[i]->GetX()+traslata_x);
        __pointsList[i]->SetY(__pointsList[i]->GetY()+traslata_y);
    }

    for(unsigned int i=0; i < __boundingBox.size();i++)
    {
        __boundingBox[i]->SetX(__boundingBox[i]->GetX()+traslata_x);
        __boundingBox[i]->SetY(__boundingBox[i]->GetY()+traslata_y);
    }
}

// creazione della bouding box, ossia del più piccolo rettangolo con i lati paralleli agli assi in grado di contenere il poligono

void IPolygon::CreateBoundingBox()
{
    float min_x = __pointsList[0]->GetX(), max_x = __pointsList[0]->GetX();
    float min_y = __pointsList[0]->GetY(), max_y = __pointsList[0]->GetY();

    // calcolo di ascissa massima/minima e di ordinata massima/minima

    for(unsigned int i=1; i<__pointsList.size();i++)
    {
        float x_punto = __pointsList[i]->GetX();
        float y_punto = __pointsList[i]->GetY();

        if(min_y > y_punto)
            min_y = y_punto;
        if(min_x > x_punto)
            min_x = x_punto;
        if(max_y < y_punto)
            max_y = y_punto;
        if(max_x < x_punto)
            max_x = x_punto;

     }

    PointFactory pointfac;
    __boundingBox.push_back(pointfac.CreatePoint(min_x, min_y));
    __boundingBox.push_back(pointfac.CreatePoint(max_x, min_y));
    __boundingBox.push_back(pointfac.CreatePoint(max_x, max_y));
    __boundingBox.push_back(pointfac.CreatePoint(min_x, max_y));
}
}
