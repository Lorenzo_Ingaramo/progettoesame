#ifndef Geometryfunction_H
#define Geometryfunction_H
#include <cmath>
#include <vector>
#include <list>
#include "Eigen"
#include <iostream>
#include <fstream>
#include "GeometryObject.hpp"
#include "Interfaces.hpp"

using namespace std;
using namespace Eigen;
using namespace GeometryObject;
using namespace Interfaces;

namespace GeometryLibrary {

  class IntersectionPoint{
    public:
      static float __tollParallelism;
      static float __tollTangent;
    private:

      static IPoint* __found;

    public:
      static TypeIntersection ComputeIntersection(const Segment& edge, const Segment& line);
      static IPoint* GetFoundPoint() {return __found;}

    private:
      static bool ComputeParallelism(const Segment& edge, const Segment& line);
      IntersectionPoint() { }
  };

  class PolygonCut{
    private:
      vector<IPolygon*> __polygonsList;
      vector<IPolygon*> __polygonToCut;

      vector<IPoint*> __foundPointsList;
      vector<IPoint*> __finalPointsList;

      vector<Segment> __intersectionSegments;
      vector<Segment> __segments;
      unsigned int __iter = 0;

    public:
      PolygonCut() {__polygonsList.reserve(1); __foundPointsList.reserve(3); __intersectionSegments.reserve(3);}
      vector<IPoint*> GetFinalPointsList();
      vector<vector<unsigned int>> GetPolygonsList();
      vector<IPolygon*>& GetPolygonsList1() {return __polygonToCut;}
      void Cut(vector<IPoint*>& points_list, vector<unsigned int>& vertices_list, vector<IPoint*>& segment_list);
      void Cut1(IPolygon* polygon, vector<IPoint*> segment_list);
    private:
      void PolygonsConstruction(vector<IPoint*>& sending_list_copy,Segment segment, IPoint* initialPoint, unsigned int index);
      void ComputeCut();
      bool CheckInside(IPoint*& p1, IPoint*& p2, unsigned int index);

  };

  class Mesh
  {

   private:
      static vector<IPolygon*> __polygonInMesh;
   public:

      static vector<IPolygon*> CreateMesh(IPolygon* domain, ReferenceElement& rf);

      static void PrintMesh();
  private:
      Mesh(){__polygonInMesh.reserve(1);}



  };
}


#endif // EMPTYCLASS_H

