#ifndef GeometryObject_H
#define GeometryObject_H
#include <cmath>
#include <vector>
#include <list>
#include "Eigen"
#include <iostream>
#include <fstream>
#include "Interfaces.hpp"

using namespace Interfaces;
using namespace std;
using namespace Eigen;

namespace GeometryObject {

class Point : public IPoint {
private:
double __x;
double __y;
unsigned int __numv;
Interfaces::TypeIntersection __type;
Interfaces::onBoundingBox __BbType;
public:
Point() {__x = 0; __y=0;}
Point(double x, double y){ __x = x;
                         __y = y;
                         __type = TypeIntersection::NOTINT;
                         __BbType = onBoundingBox::NOTINBb;
                       }

void SetVertex(unsigned int v) { __numv = v;};

double GetX() const{ return __x;}
double GetY() const{ return __y;}

void SetX(double x) {__x = x;}
void SetY(double y) {__y = y;}

unsigned int GetVertex() const { return __numv; }

void SetTypeIntersection(TypeIntersection typeint) { __type = typeint;}
TypeIntersection GetTypeIntersection() const { return __type;}

void SetBbType(onBoundingBox Bbtype) {__BbType = Bbtype;}
onBoundingBox GetBbType() const {return __BbType;}

~Point(){};
};

class PointFactory : IPointFactory
{
 public:
    ~PointFactory() {}
    IPoint* CreatePoint(double x, double y) override { return new Point(x,y);}
};


class Polygon : public IPolygon{
public:
Polygon() {}
Polygon(vector<IPoint*>& points, vector<unsigned int>& vertex, bool sort_vertex);
Polygon(const Polygon& poly);
~Polygon() {

    for(unsigned int i=0; i<__pointsList.size(); i++)
        if(__pointsList[i] != nullptr)
            delete __pointsList[i];
    for(unsigned int i=0; i < __boundingBox.size(); i++)
        if(__boundingBox[i] != nullptr)
            delete __boundingBox[i];
}


bool PointInPolygon(const IPoint& p) const {return false;}


};

class ConvexPolygon : public IPolygon{
public:
ConvexPolygon(vector<IPoint*>& points, vector<unsigned int>& vertex, bool sort_vertex);
ConvexPolygon(const Polygon& poly);
~ConvexPolygon()
{

    for(unsigned int i=0; i<__pointsList.size(); i++)
        if(__pointsList[i] != nullptr)
            delete __pointsList[i];
    for(unsigned int i=0; i < __boundingBox.size(); i++)
        if(__boundingBox[i] != nullptr)
            delete __boundingBox[i];
}

bool PointInPolygon(const IPoint& p) const;


};


class GenericPolygonFactory : public IPolygonFactory{
public:
    ~GenericPolygonFactory(){}
    IPolygon* CreatePolygon(vector<IPoint*> points,vector<unsigned int>& vertex, bool sort_vertex) {return new Polygon(points,vertex,sort_vertex);}

};

class ConvexPolygonFactory : public IPolygonFactory{
public:
    ~ConvexPolygonFactory(){}
    IPolygon* CreatePolygon(vector<IPoint*> points,vector<unsigned int>& vertex, bool sort_vertex) {return new ConvexPolygon(points,vertex,sort_vertex);}

};

class ReferenceElement
{
  private:
    vector<IPoint*> __boundingBox;
    IPolygon* __bB;
    vector<IPolygon*> __polygonsList;
    double __h;
    double __b;

  public:
    ReferenceElement(){__boundingBox.reserve(4); __polygonsList.reserve(1); __h = 0; __b = 0;}

    ReferenceElement& CreateReferenceElement(IPolygon*& polygon, vector<IPoint*> boundingbox);

    vector<IPoint*> GetBoundingBox() const {  return __boundingBox;}
    vector<IPolygon*>& GetPolygonList() {return __polygonsList;}

    float GetH() const {return __h;}
    float GetB() const {return __b;}


    void TraslateReferenceElement(double h, double b);

    ~ReferenceElement()
    {
        for(unsigned int i=0; i < __boundingBox.size(); i++)
            if(__boundingBox[i] != nullptr)
            {
                delete __boundingBox[i];
                __boundingBox[i] = nullptr;
            }
        for(unsigned int i = 0; i < __polygonsList.size(); i++)
            __polygonsList[i]->Interfaces::IPolygon::~IPolygon();
    }

};

}


#endif // EMPTYCLASS_H

