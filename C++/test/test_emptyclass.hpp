#ifndef __TEST_EMPTYCLASS_H
#define __TEST_EMPTYCLASS_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>
#include <vector>
#include "Eigen"

#include "Interfaces.hpp"
#include "Geometryfunction.hpp"
#include "GeometryObject.hpp"

using namespace GeometryObject;
using namespace GeometryLibrary;
using namespace testing;
using namespace std;
using namespace Eigen;

namespace EmptyTesting {

  TEST(TestPoint, TestGetPoint)
  {
    PointFactory pointfac;
    IPoint* point = pointfac.CreatePoint(1,1);
    try
    {
      EXPECT_EQ(point->GetX(), 1);
      EXPECT_EQ(point->GetY(), 1);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

  }

  TEST(TestPoint, TestGetVertex)
  {
    PointFactory pointfac;
    IPoint* point = pointfac.CreatePoint(1,1);
    point->SetVertex(5);
    try
    {
      EXPECT_EQ(point->GetVertex(), 5);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

  }

  TEST(TestSegment, TestGetDirection)
  {
    PointFactory pointfac;
    IPoint* point1 = pointfac.CreatePoint(1,1);
    IPoint* point2 = pointfac.CreatePoint(1+5e-7,1+1e-7);
    IPoint* point3 = pointfac.CreatePoint(4,5);
    try
    {
      Segment segment1 = Segment(point1, point2);
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Segmento non creato: punti troppo vicini"));
    }
    try
    {
      Segment segment2 = Segment(point1, point3);
      EXPECT_EQ(segment2.GetDirection()[0], 3);
      EXPECT_EQ(segment2.GetDirection()[1], 4);
      EXPECT_EQ(segment2.GetNormalDirection()[0], 4);
      EXPECT_EQ(segment2.GetNormalDirection()[1], -3);
    }
    catch (const exception& exception)
    {
      FAIL();
    }
  }

  TEST(TestSegment, TestGetExtremePoints)
  {
    PointFactory pointfac;
    IPoint* point = pointfac.CreatePoint(1,1);
    IPoint* point2 = pointfac.CreatePoint(4,5);
    Segment segment = Segment(point, point2);
    try
    {
      EXPECT_EQ(segment.GetBegin()->GetX(), 1);
      EXPECT_EQ(segment.GetBegin()->GetY(), 1);
      EXPECT_EQ(segment.GetEnd()->GetX(), 4);
      EXPECT_EQ(segment.GetEnd()->GetY(), 5);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

  }

  TEST(TestPolygon, TestGetPoints)
  {
      PointFactory pointfac;
    IPoint* point1 = pointfac.CreatePoint(0,8);
    IPoint* point2 = pointfac.CreatePoint(2,5);
    IPoint* point3 = pointfac.CreatePoint(1,1);
    IPoint* point4 = pointfac.CreatePoint(3,3);
    vector<unsigned int> vertices_list ={3, 2, 0, 1};
    vector<IPoint*> points_list ={point1, point2, point3, point4};
    Polygon polygon(points_list, vertices_list, true);
    try
    {
      EXPECT_EQ(polygon.GetPointsList()[0]->GetX(), 1);
      EXPECT_EQ(polygon.GetPointsList()[0]->GetY(), 1);
      EXPECT_EQ(polygon.GetPointsList()[1]->GetX(), 3);
      EXPECT_EQ(polygon.GetPointsList()[1]->GetY(), 3);
      EXPECT_EQ(polygon.GetPointsList()[2]->GetX(), 2);
      EXPECT_EQ(polygon.GetPointsList()[2]->GetY(), 5);
      EXPECT_EQ(polygon.GetPointsList()[3]->GetX(), 0);
      EXPECT_EQ(polygon.GetPointsList()[3]->GetY(), 8);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

  }

  TEST(TestPolygon, TestGetSegments)
  {
    PointFactory pointfac;
    IPoint* point3 = pointfac.CreatePoint(3,3);
    IPoint* point2 = pointfac.CreatePoint(2,5);
    IPoint* point1 = pointfac.CreatePoint(1,1);
    vector<unsigned int> vertices_list ={0, 2, 1};
    vector<IPoint*> points_list ={point1, point2, point3};
    Polygon polygon(points_list, vertices_list, true);
    vector<IPoint*> points = polygon.GetPointsList();
    try
    {
        EXPECT_EQ(*polygon.GetSegmentsList()[0].GetBegin(), *points[0]);
        EXPECT_EQ(polygon.GetSegmentsList()[0].GetDirection()[0], points[1]->GetX()-points[0]->GetX());
        EXPECT_EQ(polygon.GetSegmentsList()[0].GetDirection()[1], points[1]->GetY()-points[0]->GetY());
        EXPECT_EQ(*polygon.GetSegmentsList()[1].GetBegin(), *points[1]);
        EXPECT_EQ(polygon.GetSegmentsList()[1].GetDirection()[0], points[2]->GetX()-points[1]->GetX());
        EXPECT_EQ(polygon.GetSegmentsList()[1].GetDirection()[1], points[2]->GetY()-points[1]->GetY());
        EXPECT_EQ(*polygon.GetSegmentsList()[2].GetBegin(), *points[2]);
        EXPECT_EQ(polygon.GetSegmentsList()[2].GetDirection()[0], points[0]->GetX()-points[2]->GetX());
        EXPECT_EQ(polygon.GetSegmentsList()[2].GetDirection()[1], points[0]->GetY()-points[2]->GetY());
    }
    catch (const exception& exception)
    {
      FAIL();
    }

  }

  TEST(TestIntersectionPoint, TestComputeIntersection)
  {
    PointFactory pointfac;
    IPoint* p3 = pointfac.CreatePoint(3,5);
    IPoint* p2 = pointfac.CreatePoint(4,5);
    IPoint* p1 = pointfac.CreatePoint(1,1);
    IPoint* p4 = pointfac.CreatePoint(6,9);
    IPoint* p5 = pointfac.CreatePoint(-(0.5*11),9);
    IPoint* p6 = pointfac.CreatePoint(-(0.5*3),6);
    Segment seg1 = Segment(p1,p2);
    Segment seg2 = Segment(p3, p4);
    Segment seg3 = Segment(p5, p6);
    try
    {
        EXPECT_EQ(IntersectionPoint::ComputeIntersection(seg1, seg2), TypeIntersection::NOTINT);
    }
    catch (const exception& exception)
    {
      FAIL();
    }
    try
    {
        EXPECT_EQ(IntersectionPoint::ComputeIntersection(seg1, seg3), TypeIntersection::PROPERINT);
        IPoint* intpoint = IntersectionPoint::GetFoundPoint();
        EXPECT_TRUE(abs(intpoint->GetX() - 0.5*5) < 1e-6);
        EXPECT_TRUE(abs(intpoint->GetY() - 3) < 1e-6);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

  }

  TEST(TestPolygonCut, TestRectangle)
  {
    PointFactory pointfac;
    IPoint* p1 = pointfac.CreatePoint(1,1);
    IPoint* p2 = pointfac.CreatePoint(5,1);
    IPoint* p3 = pointfac.CreatePoint(5,3.1);
    IPoint* p4 = pointfac.CreatePoint(1,3.1);
    IPoint* s1 = pointfac.CreatePoint(2,1.2);
    IPoint* s2 = pointfac.CreatePoint(4,3);
    PolygonCut rectanglecut;
    IPoint* int1 = pointfac.CreatePoint(1.7777777,1);
    IPoint* int2 = pointfac.CreatePoint(4.1111111,3.1);
    vector<IPoint*> points_list = {p1, p2, p3, p4};
    vector<IPoint*> points = {p1, p4, int1, s1, s2, int2, p2, p3};
    vector<unsigned int> vertices_list = {0, 1, 2, 3};
    vector<vector<unsigned int>> vertices = {{4, 1, 2, 5, 7, 6}, {0, 4, 6, 7, 5, 3}};
    vector<IPoint*> segment_points_list = {s1, s2};
    try
    {
        rectanglecut.Cut(points_list, vertices_list, segment_points_list);
        for(unsigned int i = 0; i < points.size(); i++)
        {
        EXPECT_EQ(*rectanglecut.GetFinalPointsList()[i], *points[i]);
        }
        EXPECT_EQ(rectanglecut.GetPolygonsList(), vertices);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

  }

  TEST(TestPolygonCut, TestPentagon)
  {
    Point* p1 = new Point(2.5,1);
    Point* p2 = new Point(4,2.1);
    Point* p3 = new Point(3.4,4.2);
    Point* p4 = new Point(1.6,4.2);
    Point* p5 = new Point(1,2.1);
    Point* s1 = new Point(1.4,2.75);
    Point* s2 = new Point(3.6,2.2);
    PolygonCut pentagoncut;
    Point* int1 = new Point(1.2,2.8);
    vector<IPoint*> points_list = {p1, p2, p3, p4, p5};
    vector<unsigned int> vertices_list = {0, 1, 2, 3, 4};
    vector<IPoint*> points = {p5, int1, s1, p4, p1, p3, s2, p2};
    vector<vector<unsigned int>> vertices = {{1, 2, 3, 5, 6, 7}, {0, 1, 7, 6, 5, 4}};

    vector<IPoint*> segment_points_list = {s1, s2};
    try
    {
        pentagoncut.Cut(points_list, vertices_list, segment_points_list);
        for(unsigned int i = 0; i < points.size(); i++)
        {
         EXPECT_EQ(*pentagoncut.GetFinalPointsList()[i], *points[i]);
        }
        EXPECT_EQ(pentagoncut.GetPolygonsList(), vertices);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

  }

  TEST(TestPolygonCut, TestConcave1)
  {
    Point* p1 = new Point(1.5,1);
    Point* p2 = new Point(5.6,1.5);
    Point* p3 = new Point(5.5,4.8);
    Point* p4 = new Point(4.0,6.2);
    Point* p5 = new Point(3.2,4.2);
    Point* p6 = new Point(1,4);
    Point* s1 = new Point(2,3.7);
    Point* s2 = new Point(4.1,5.9);
    Point* s1cut2 = new Point(1,4);
    Point* s2cut2 = new Point(4.0,6.2);
    PolygonCut concave1cut;
    Point* int1 = new Point(4.2043269,6.0092949);
    Point* int2 = new Point(3.7213115,5.5032787);
    Point* int3 = new Point(2.4085973,4.1280543);
    Point* int4 = new Point(1.1912162,2.8527027);
    vector<IPoint*> points_list = {p1, p2, p3, p4, p5, p6};
    vector<unsigned int> vertices_list = {0, 1, 2, 3, 4, 5};
    vector<IPoint*> points = {p6, int4, p1, s1, int3, p5, int2, p4, s2, int1, p3, p2};
    vector<vector<unsigned int>> verteces1 = {{6, 3, 7, 11}, {8, 5, 9, 10}, {0, 1, 2, 6, 11, 7, 4, 8, 10, 9}};
    vector<vector<unsigned int>> vertices2 = {{0, 1, 2, 3, 4, 5}};
    vector<IPoint*> segment_points_list = {s1, s2};
    vector<IPoint*> segment_points_list2 = {s1cut2, s2cut2};
    try
    {
        concave1cut.Cut(points_list, vertices_list, segment_points_list);
        for(unsigned int i = 0; i < points.size(); i++)
        {
         EXPECT_EQ(*concave1cut.GetFinalPointsList()[i], *points[i]);
        }
        EXPECT_EQ(concave1cut.GetPolygonsList(), verteces1);
    }
    catch (const exception& exception)
    {
      FAIL();
    }
    try
    {
        concave1cut.Cut(points_list, vertices_list, segment_points_list2);
        sort(points_list.begin(), points_list.end(), IPoint::cmp);
        for(unsigned int i = 0; i < points_list.size(); i++)
        {
         EXPECT_EQ(*concave1cut.GetFinalPointsList()[i], *points_list[i]);
        }
        EXPECT_EQ(concave1cut.GetPolygonsList(), vertices2);
    }
    catch (const exception& exception)
    {
      FAIL();
    }
  }

  TEST(TestPolygonCut, TestConvex)
  {
    Point* p1 =new Point(-7.580132,6.715825);
    Point* p2 =new Point(-4.953247,-2.040458);
    Point* p3 =new Point(4.847055,0.081257);
    Point* p4 =new Point(10.336572,7.355708);
    Point* p5 =new Point(2.961087,12.171665);
    Point* s1 =new Point(-8.523117,3.415380);
    Point* s2 =new Point(9.730368,1.933548);
    PolygonCut convexcut;
    Point* int1 =new Point(6.446051,2.200171);
    Point* int2 =new Point(-6.541744,3.254530);
    vector<IPoint*> points_list = {p1, p2, p3, p4, p5};
    vector<unsigned int> vertices_list = {4, 0, 1, 2, 3};
    vector<IPoint*> points ={s1, p5, int2, p1, p4, p2, int1, p3, s2};
    sort(points.begin(), points.end());
    vector<vector<unsigned int>> verteces = {{5, 2, 3, 4, 6}, {0, 1, 5, 6}};
    vector<IPoint*> segment_points_list = {s1, s2};
    try
    {
        convexcut.Cut(points_list, vertices_list, segment_points_list);
        sort(points.begin(), points.end(), IPoint::cmp);
        for(unsigned int i = 0; i < points.size(); i++)
        {
         EXPECT_EQ(*convexcut.GetFinalPointsList()[i], *points[i]);
        }
        EXPECT_EQ(convexcut.GetPolygonsList(), verteces);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

  }

  TEST(TestPolygonCut, TestConcave2)
  {
    Point* p1 = new Point(-1.787253,12.423857);
    Point* p2 = new Point(8.072415,6.807078);
    Point* p3 = new Point(-5.828101,0);
    Point* p4 = new Point(6.254034,-6.164044);
    Point* p5 = new Point(-6.959539,-12.993077);
    Point* p6 = new Point(0,-7.618749);
    Point* p7 = new Point(-10.959978,0);
    Point* p8 = new Point(2.276637,6.740621);
    Point* s1 = new Point(7.053441,12.393031);
    Point* s2 = new Point(-6.741197,-16.521709);
    PolygonCut concave2cut;
    Point* int1 = new Point(5.175749,8.457228);
    Point* int2 = new Point(3.265469,4.453118);
    Point* int3 = new Point(-0.223227,-2.859486);
    Point* int4 = new Point(-1.872712,-6.316947);
    Point* int5 = new Point(-3.948426,-10.667821);
    Point* int6 = new Point(-4.435395,-11.688550);
    vector<IPoint*> points_list = {p1, p2, p3, p4, p5, p6, p7, p8};
    vector<unsigned int> vertices_list = {4, 3, 2, 1, 0, 7, 6, 5};
    vector<IPoint*> points = {p5, int6, p4, int3, p3, int2, p2, int1, p1, p8, p7, int4, p6, int5, s1, s2};
    sort(points.begin(), points.end());
    vector<vector<unsigned int>> verteces = {{10, 3, 11}, {9, 2, 10, 11, 4, 5, 6, 12}, {8, 1, 9, 12, 7, 13}, {0, 8, 13}};
    vector<IPoint*> segment_points_list = {s1, s2};
    try
    {
        concave2cut.Cut(points_list, vertices_list, segment_points_list);
        sort(points.begin(), points.end(), IPoint::cmp);
        for(unsigned int i = 0; i < points.size(); i++)
        {
         EXPECT_EQ(*concave2cut.GetFinalPointsList()[i], *points[i]);
        }
        EXPECT_EQ(concave2cut.GetPolygonsList(), verteces);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

  }
  TEST(TestReferenceElement, TestRfrectangle)
  {
      Point* p1 =new Point(1,1);
      Point* p2 =new Point(5,1);
      Point* p3 =new Point(5,3.1);
      Point* p4 =new Point(1,3.1);
      vector<IPoint*> points_list = {p1, p2, p3, p4};
      ConvexPolygonFactory polyfac;
      vector<unsigned int> vertices_list = {0, 1, 2, 3};
      IPolygon* rectangle = polyfac.CreatePolygon(points_list,vertices_list, true);
      vector <IPoint*> Bb = rectangle->GetBoundingBox();
      vector <IPolygon*> Rflist;
      ReferenceElement rf;
      double surface = rectangle->ComputeArea();
      double polysurface = 0;
      try
      {
          rf = rf.CreateReferenceElement(rectangle,Bb);
          Rflist = rf.GetPolygonList();
          for(unsigned int i = 0; i< Rflist.size(); i++)
              polysurface += Rflist[i]->ComputeArea();

          EXPECT_TRUE(abs(polysurface - surface)/(surface)<1e-6);
      }
      catch (const exception& exception)
      {
          FAIL();
      }
  }
  TEST(TestReferenceElement, TestRfPentagon)
  {
      Point* p1 = new Point(2.5,1);
      Point* p2 = new Point(4,2.1);
      Point* p3 = new Point(3.4,4.2);
      Point* p4 = new Point(1.6,4.2);
      Point* p5 = new Point(1,2.1);
      vector <IPoint*> pointlist = {p1,p2,p3,p4,p5};
      vector <unsigned int> vertexlist = {0,1,2,3,4};
      ConvexPolygonFactory polyfac;
      IPolygon* pentagon = polyfac.CreatePolygon(pointlist,vertexlist, false);
      vector <IPoint*> Bb = pentagon->GetBoundingBox();
      vector <IPolygon*> Rflist;
      ReferenceElement rf;
      double surface = 9.6;
      double polysurface = 0;
      try
      {
          rf = rf.CreateReferenceElement(pentagon,Bb);
          Rflist = rf.GetPolygonList();
          for(unsigned int i = 0; i< Rflist.size(); i++)
              polysurface += Rflist[i]->ComputeArea();

          EXPECT_TRUE(abs(polysurface - surface)/surface<1e-6);
      }
      catch (const exception& exception)
      {
          FAIL();
      }
  }
  TEST(TestReferenceElement, TestRfConcave1)
  {
      Point* p1 = new Point(1.5,1);
      Point* p2 = new Point(5.6,1.5);
      Point* p3 = new Point(5.5,4.8);
      Point* p4 = new Point(4.0,6.2);
      Point* p5 = new Point(3.2,4.2);
      Point* p6 = new Point(1,4);
      vector<IPoint*> points_list = {p1, p2, p3, p4, p5, p6};
      vector<unsigned int> vertices_list = {0, 1, 2, 3, 4, 5};
      GenericPolygonFactory polyfac;
      IPolygon* concave1 = polyfac.CreatePolygon(points_list,vertices_list,true);
      vector <IPoint*> Bb = concave1->GetBoundingBox();
      vector <IPolygon*> Rflist;
      ReferenceElement rf;
      double polysurface = 0;
      double surface = 23.92;
      try
      {
          rf = rf.CreateReferenceElement(concave1,Bb);
          Rflist = rf.GetPolygonList();
          for(unsigned int i = 0; i< Rflist.size(); i++)
              polysurface += Rflist[i]->ComputeArea();
          EXPECT_TRUE(abs(polysurface - surface)/surface<1e-6);
      }
      catch (const exception& exception)
      {
          FAIL();
      }
  }
  TEST(TestMesh, Testrectangledomain)
  {
      Point* p1 = new Point(1.5,1);
      Point* p2 = new Point(5.6,1.5);
      Point* p3 = new Point(5.5,4.8);
      Point* p4 = new Point(4.0,6.2);
      Point* p5 = new Point(3.2,4.2);
      Point* p6 = new Point(1,4);
      Point* p1dom = new Point(0,0);
      Point* p2dom = new Point(4*4.6+1,0);
      Point* p3dom = new Point(4*4.6+1,3*5.2+1);
      Point* p4dom = new Point(0,3*5.2+1);

      vector<IPoint*> points_list = {p1, p2, p3, p4, p5, p6};
      vector<IPoint*> points_list_domain = {p1dom, p2dom, p3dom, p4dom};
      vector<unsigned int> vertices_list = {0, 1, 2, 3, 4, 5};
      vector<unsigned int> vertices_list_domain = {0,1,2,3};

      ConvexPolygonFactory polyfac1;
      GenericPolygonFactory polyfac2;

      IPolygon* concave1 = polyfac2.CreatePolygon(points_list,vertices_list,true);
      IPolygon* domain = polyfac1.CreatePolygon(points_list_domain, vertices_list_domain, true);

      vector <IPoint*> Bb = concave1->GetBoundingBox();
      ReferenceElement rf;
      rf = rf.CreateReferenceElement(concave1,Bb);

      double surfacedomain = domain->ComputeArea();
      double surface = 0;
      try
      {
          vector<IPolygon*> polygon_in_mesh = Mesh::CreateMesh(domain,rf);
          for(unsigned int i = 0; i < polygon_in_mesh.size(); i++)
            surface += polygon_in_mesh[i]->ComputeArea();
          EXPECT_TRUE(abs(surfacedomain - surface)/surface<1e-6);
      }
      catch (const exception& exception)
      {
          FAIL();
      }
  }
  TEST(TestMesh, Testconvexdomain)
  {
      Point* p1 = new Point(1.5,1);
      Point* p2 = new Point(5.6,1.5);
      Point* p3 = new Point(5.5,4.8);
      Point* p4 = new Point(4.0,6.2);
      Point* p5 = new Point(3.2,4.2);
      Point* p6 = new Point(1,4);

      Point* p1dom =new Point(0, 0);
      Point* p2dom =new Point(16, 4);
      Point* p3dom =new Point(19, 22);
      Point* p4dom =new Point(3, 17);

      vector<IPoint*> points_list = {p1, p2, p3, p4, p5, p6};
      vector<IPoint*> points_list_domain = {p1dom, p2dom, p3dom, p4dom};
      vector<unsigned int> vertices_list = {0, 1, 2, 3, 4, 5};
      vector<unsigned int> vertices_list_domain = {0,1,2,3};

      ConvexPolygonFactory polyfac1;
      GenericPolygonFactory polyfac2;

      IPolygon* concave1 = polyfac2.CreatePolygon(points_list,vertices_list,true);
      IPolygon* domain = polyfac1.CreatePolygon(points_list_domain, vertices_list_domain, true);

      vector <IPoint*> Bb = concave1->GetBoundingBox();
      ReferenceElement rf;
      rf = rf.CreateReferenceElement(concave1,Bb);

      double surfacedomain = domain->ComputeArea();
      double surface = 0;


      try
      {
          vector<IPolygon*> polygon_in_mesh = Mesh::CreateMesh(domain,rf);
          for(unsigned int i = 0; i < polygon_in_mesh.size(); i++)
            surface += polygon_in_mesh[i]->ComputeArea();
          EXPECT_TRUE(abs(surfacedomain - surface)/surface<1e-6);
      }
      catch (const exception& exception)
      {
          FAIL();
      }
  }
  TEST(TestMesh, Testconvexdomain1)
  {
      Point* p1 = new Point(2.5,1);
      Point* p2 = new Point(4,2.1);
      Point* p3 = new Point(3.4,4.2);
      Point* p4 = new Point(1.6,4.2);
      Point* p5 = new Point(1,2.1);

      Point* p1dom =new Point(0, 0);
      Point* p2dom =new Point(16, 4);
      Point* p3dom =new Point(19, 22);
      Point* p4dom =new Point(3, 17);

      vector<IPoint*> points_list = {p1, p2, p3, p4, p5};
      vector<IPoint*> points_list_domain = {p1dom, p2dom, p3dom, p4dom};
      vector<unsigned int> vertices_list = {0, 1, 2, 3, 4};
      vector<unsigned int> vertices_list_domain = {0,1,2,3};

      ConvexPolygonFactory polyfac1;
      GenericPolygonFactory polyfac2;

      IPolygon* concave1 = polyfac2.CreatePolygon(points_list,vertices_list,true);
      IPolygon* domain = polyfac1.CreatePolygon(points_list_domain, vertices_list_domain, true);

      vector <IPoint*> Bb = concave1->GetBoundingBox();
      ReferenceElement rf;
      rf = rf.CreateReferenceElement(concave1,Bb);

      double surfacedomain = domain->ComputeArea();
      double surface = 0;

      try
      {
          vector<IPolygon*> polygon_in_mesh = Mesh::CreateMesh(domain,rf);
          for(unsigned int i = 0; i < polygon_in_mesh.size(); i++)
            surface += polygon_in_mesh[i]->ComputeArea();
          Mesh::PrintMesh();
          EXPECT_TRUE(abs(surfacedomain - surface)/surface<1e-6);
      }
      catch (const exception& exception)
      {
          FAIL();
      }
  }

  TEST(TestMesh, Testconvexdomain2)
  {
      Point* p1 = new Point(1.5,1);
      Point* p2 = new Point(5.6,1.5);
      Point* p3 = new Point(5.5,4.8);
      Point* p4 = new Point(4.0,6.2);
      Point* p5 = new Point(3.2,4.2);
      Point* p6 = new Point(1,4);
      Point* p1dom =new Point(0, 0);
      Point* p2dom =new Point(40, 12);
      Point* p3dom =new Point(50, 42);
      Point* p4dom =new Point(13, 35);

      vector<IPoint*> points_list = {p1, p2, p3, p4, p5, p6};
      vector<IPoint*> points_list_domain = {p1dom, p2dom, p3dom, p4dom};
      vector<unsigned int> vertices_list = {0, 1, 2, 3, 4, 5};
      vector<unsigned int> vertices_list_domain = {0,1,2,3};

      ConvexPolygonFactory polyfac1;
      GenericPolygonFactory polyfac2;

      IPolygon* concave1 = polyfac2.CreatePolygon(points_list,vertices_list,true);
      IPolygon* domain = polyfac1.CreatePolygon(points_list_domain, vertices_list_domain, true);

      vector <IPoint*> Bb = concave1->GetBoundingBox();
      ReferenceElement rf;
      rf = rf.CreateReferenceElement(concave1,Bb);

      double surfacedomain = domain->ComputeArea();
      double surface = 0;

      try
      {
          vector<IPolygon*> polygon_in_mesh = Mesh::CreateMesh(domain,rf);
           for(unsigned int i = 0; i < polygon_in_mesh.size(); i++)
            surface += polygon_in_mesh[i]->ComputeArea();
          Mesh::PrintMesh();
          EXPECT_TRUE(abs(surfacedomain - surface)/surface<1e-6);
      }
      catch (const exception& exception)
      {
          FAIL();
      }
  }


  TEST(TestMesh, Testdisegnodocumentazione)
  {
      Point* p1 = new Point(3.2, 2.0);
      Point* p2 = new Point(5.7, 4.0);
      Point* p3 = new Point(4.7, 6.0);
      Point* p4 = new Point(2.5, 4.0);
      Point* p1dom =new Point(2.5, 2.5);
      Point* p2dom =new Point(16.0, 2.0);
      Point* p3dom =new Point(22.0, 9.0);
      Point* p4dom =new Point(16.9, 14.0);
      Point* p5dom =new Point(6.0, 12.5);

      vector<IPoint*> points_list = {p1, p2, p3, p4};
      vector<IPoint*> points_list_domain = {p1dom, p2dom, p3dom, p4dom, p5dom};
      vector<unsigned int> vertices_list = {0, 1, 2, 3};
      vector<unsigned int> vertices_list_domain = {0,1,2,3,4};

      ConvexPolygonFactory polyfac1;
      GenericPolygonFactory polyfac2;

      IPolygon* concave1 = polyfac2.CreatePolygon(points_list,vertices_list,true);
      IPolygon* domain = polyfac1.CreatePolygon(points_list_domain, vertices_list_domain, true);

      vector <IPoint*> Bb = concave1->GetBoundingBox();
      ReferenceElement rf;
      rf = rf.CreateReferenceElement(concave1,Bb);

      double surfacedomain = domain->ComputeArea();
      double surface = 0;

      try
      {
          vector<IPolygon*> polygon_in_mesh = Mesh::CreateMesh(domain,rf);
          for(unsigned int i = 0; i < polygon_in_mesh.size(); i++)
            surface += polygon_in_mesh[i]->ComputeArea();
          Mesh::PrintMesh();
          EXPECT_TRUE(abs(surfacedomain - surface)/surface<1e-6);
      }
      catch (const exception& exception)
      {
          FAIL();
      }
  }


}

#endif // __TEST_EMPTYCLASS_H


