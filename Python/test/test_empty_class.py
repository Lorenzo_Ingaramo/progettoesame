from unittest import TestCase

import src.empty_class as geometry_library
import numpy as np


class TestNormalPoint(TestCase):
    def test_get_point(self):
        point = geometry_library.Point(1, 1)
        self.assertEqual(point.get_x(), 1)
        self.assertEqual(point.get_y(), 1)

    def test_get_vertex(self):
        point = geometry_library.Point(1, 1)
        point.set_vertex(3)
        self.assertEqual(point.get_vertex(), 3)

class TestSegment(TestCase):
    def test_get_direction(self):
        p1 = geometry_library.Point(1, 1)
        p2 = geometry_library.Point(1+5e-7, 1+1e-7)
        p3 = geometry_library.Point(4, 5)
        try:
            segment = geometry_library.Segment(p1, p2)

        except Exception as ex:
            self.assertEqual(str(ex), "segmento non creato: punti troppo vicini")

        try:
            segment = geometry_library.Segment(p1, p3)
            self.assertEqual(segment.get_direction()[0], 3)
            self.assertEqual(segment.get_direction()[1], 4)
            self.assertEqual(segment.get_normal_direction()[0], 4)
            self.assertEqual(segment.get_normal_direction()[1], -3)

        except Exception as ex:
            self.fail()


    def test_get_begin(self):
        p1 = geometry_library.Point(1, 1)
        p2 = geometry_library.Point(4, 5)
        segment = geometry_library.Segment(p1, p2)
        self.assertEqual(segment.get_begin().get_x(), 1)
        self.assertEqual(segment.get_begin().get_y(), 1)

    def test_get_end(self):
        p1 = geometry_library.Point(1, 1)
        p2 = geometry_library.Point(4, 5)
        segment = geometry_library.Segment(p1, p2)
        self.assertEqual(segment.get_end().get_x(), 4)
        self.assertEqual(segment.get_end().get_y(), 5)

class TestPolygon(TestCase):
    def test_get_points_list(self):
        p1 = geometry_library.Point(0, 8)
        p2 = geometry_library.Point(2, 5)
        p3 = geometry_library.Point(1, 1)
        p4 = geometry_library.Point(3, 3)
        vertices_list = [3, 2, 0, 1]
        polygon = geometry_library.Polygon([p1, p2, p3, p4], vertices_list)
        self.assertEqual(polygon.get_points_list()[0].get_x(), 1)
        self.assertEqual(polygon.get_points_list()[0].get_y(), 1)
        self.assertEqual(polygon.get_points_list()[1].get_x(), 3)
        self.assertEqual(polygon.get_points_list()[1].get_y(), 3)
        self.assertEqual(polygon.get_points_list()[2].get_x(), 2)
        self.assertEqual(polygon.get_points_list()[2].get_y(), 5)
        self.assertEqual(polygon.get_points_list()[3].get_x(), 0)
        self.assertEqual(polygon.get_points_list()[3].get_y(), 8)

    def test_get_segments_list(self):
        p1 = geometry_library.Point(1, 1)
        p2 = geometry_library.Point(2, 5)
        p3 = geometry_library.Point(3, 3)
        polygon = geometry_library.Polygon([p1, p2, p3], [0, 2, 1])
        i: int = 0
        points = polygon.get_points_list()
        l = len(points)
        for i in range(0, l):
            self.assertEqual(polygon.get_segments_list()[i].get_begin(), points[i])
            self.assertEqual(polygon.get_segments_list()[i].get_direction()[0], points[(i+1)%l].get_x()-points[i].get_x())
            self.assertEqual(polygon.get_segments_list()[i].get_direction()[1], points[(i+1)%l].get_y()-points[i].get_y())

class TestIntersectionPoint(TestCase):
    def test_compute_intersection(self):
        p1 = geometry_library.Point(1, 1)
        p2 = geometry_library.Point(4, 5)
        p3 = geometry_library.Point(3, 5)
        p4 = geometry_library.Point(6, 9)
        p5 = geometry_library.Point(-11/2, 9)
        p6 = geometry_library.Point(-3/2, 6)
        seg1 = geometry_library.Segment(p1, p2)
        seg2 = geometry_library.Segment(p3, p4)
        seg3 = geometry_library.Segment(p5, p6)
        intersection = geometry_library.IntersectionPoint()
        intersection.set_tolerance_tangent(1e-6)
        intersection.set_tolerance_parallelism(1e-6)
        try:
            self.assertEqual(intersection.compute_intersection(seg1, seg2), geometry_library.TypeIntersection.NOTINT)
        except Exception as ex:
            self.fail()

        try:
            self.assertEqual(intersection.compute_intersection(seg1, seg3), geometry_library.TypeIntersection.PROPERINT)
            intpoint : geometry_library.Point = intersection.get_found_point()
            self.assertEqual(intpoint.get_x(), 5/2)
            self.assertEqual(intpoint.get_y(), 3)

        except Exception as ex:
            self.fail()

class TestPolygonCut(TestCase):
    def test_rectangle(self):
        p1 = geometry_library.Point(1, 1)
        p2 = geometry_library.Point(5, 1)
        p3 = geometry_library.Point(5, 3.1)
        p4 = geometry_library.Point(1, 3.1)
        s1 = geometry_library.Point(2, 1.2)
        s2 = geometry_library.Point(4, 3)
        rectanglecut = geometry_library.PolygonCut()
        int1 = geometry_library.Point(1.7777777777, 1)
        int2 = geometry_library.Point(4.11111111111, 3.1)
        points_list = [p1, p2, p3, p4]
        vertices_list = [0, 1, 2, 3]
        segment_points_list = [s1, s2]
        try:
            rectanglecut.cut(points_list, vertices_list, segment_points_list)
            self.assertEqual(rectanglecut.get_final_points_list(), [p1, int1, p2, p3, int2, p4])
            self.assertEqual(rectanglecut.get_final_polygon_list(), [[4, 1, 2, 5, 6, 7], [0, 4, 7, 6, 5, 3]])
        except Exception as ex:
            self.fail()

    def test_pentagon(self):
        p1 = geometry_library.Point(2.5, 1)
        p2 = geometry_library.Point(4, 2.1)
        p3 = geometry_library.Point(3.4, 4.2)
        p4 = geometry_library.Point(1.6, 4.2)
        p5 = geometry_library.Point(1, 2.1)
        s1 = geometry_library.Point(1.4, 2.75)
        s2 = geometry_library.Point(3.6, 2.2)
        pentagoncut = geometry_library.PolygonCut()
        int1 = geometry_library.Point(1.2,2.8)
        int2 = p2
        points_list = [p1, p2, p3, p4, p5]
        vertices_list = [0, 1, 2, 3, 4]
        segment_points_list = [s1, s2]
        try:
            pentagoncut.cut(points_list, vertices_list, segment_points_list)
            self.assertEqual(pentagoncut.get_final_points_list(), [p1, p2, p3, p4, int1, p5])
            self.assertEqual(pentagoncut.get_final_polygon_list(), [[1, 2, 3, 5, 7, 6], [0, 1, 6, 7, 5, 4]])
        except Exception as ex:
            self.fail()

    def test_concave1(self):
        p1 = geometry_library.Point(1.5,1)
        p2 = geometry_library.Point(5.6,1.5)
        p3 = geometry_library.Point(5.5,4.8)
        p4 = geometry_library.Point(4.0,6.2)
        p5 = geometry_library.Point(3.2,4.2)
        p6 = geometry_library.Point(1,4)
        s1 = geometry_library.Point(2,3.7)
        s2 = geometry_library.Point(4.1,5.9)
        concavecut1 = geometry_library.PolygonCut()
        concavecut2 = geometry_library.PolygonCut()
        int1 = geometry_library.Point(4.204326923,6.009294872)
        int2 = geometry_library.Point(3.721311475, 5.50327869)
        int3 = geometry_library.Point(2.408597285, 4.128054299)
        int4 = geometry_library.Point(1.1912162162, 2.852702703)
        points_list = [p1, p2, p3, p4, p5, p6]
        s1_cut2 = geometry_library.Point(1,4)
        s2_cut2 = geometry_library.Point(4.0,6.2)
        vertices_list = [0, 1, 2, 3, 4, 5]
        segment_points_list = [s1, s2]
        try:
            concavecut1.cut(points_list, vertices_list, segment_points_list)
            self.assertEqual(concavecut1.get_final_points_list(), [p1, p2, p3, int1, p4, int2, p5, int3, p6, int4])
            self.assertEqual(concavecut1.get_final_polygon_list(), [[6, 3, 7, 10], [8, 5, 9, 11], [0, 1, 2, 6, 10, 7, 4, 8, 11, 9]])
        except Exception as ex:
            self.fail()
        try:
            concavecut2.cut(points_list,vertices_list, [s1_cut2, s2_cut2])  #taglio con una retta passante solo per 2 vertici
            self.assertEqual(concavecut2.get_final_points_list(), points_list)
            self.assertEqual(concavecut2.get_final_polygon_list(),[vertices_list])
        except Exception as ex:
            self.fail()

    def test_convex(self):
        p1 = geometry_library.Point(-7.580132,6.715825)
        p2 = geometry_library.Point(-4.953247,-2.040458)
        p3 = geometry_library.Point(4.847055,0.081257)
        p4 = geometry_library.Point(10.336572,7.355708)
        p5 = geometry_library.Point(2.961087,12.171665)
        s1 = geometry_library.Point(-8.523117,3.415380)
        s2 = geometry_library.Point(9.730368,1.933548)
        convexcut = geometry_library.PolygonCut()
        int1 = geometry_library.Point(6.446051,2.200171)
        int2 = geometry_library.Point(-6.541744,3.254530)
        points_list = [p1, p2, p3, p4, p5]
        vertices_list = [4, 0, 1, 2, 3]
        segment_points_list = [s1, s2]
        try:
            convexcut.cut(points_list, vertices_list, segment_points_list)
            self.assertEqual(convexcut.get_final_points_list(), [p2, p3, int1, p4, p5, p1, int2])
            self.assertEqual(convexcut.get_final_polygon_list(), [[5, 2, 3, 4, 6], [0, 1, 5, 6]])
        except Exception as ex:
            self.fail()

    def test_concave2(self):
        p1 = geometry_library.Point(-1.787253,12.423857)
        p2 = geometry_library.Point(8.072415,6.807078)
        p3 = geometry_library.Point(-5.828101,0)
        p4 = geometry_library.Point(6.254034,-6.164044)
        p5 = geometry_library.Point(-6.959539,-12.993077)
        p6 = geometry_library.Point(0,-7.618749)
        p7 = geometry_library.Point(-10.959978,0)
        p8 = geometry_library.Point(2.276637,6.740621)
        s1 = geometry_library.Point(7.053441,12.393031)
        s2 = geometry_library.Point(-6.741197,-16.521709)
        concavecut2 = geometry_library.PolygonCut()
        int1 = geometry_library.Point(5.175749,8.457228)
        int2 = geometry_library.Point(3.265469,4.453118)
        int3 = geometry_library.Point(-0.223227,-2.859486)
        int4 = geometry_library.Point(-1.872712,-6.316947)
        int5 = geometry_library.Point(-3.948426,-10.667821)
        int6 = geometry_library.Point(-4.435395,-11.688550)
        points_list = [p1, p2, p3, p4, p5, p6, p7, p8]
        vertices_list = [4, 3, 2, 1, 0, 7, 6, 5]
        segment_points_list = [s1, s2]
        try:
            concavecut2.cut(points_list, vertices_list, segment_points_list)
            self.assertEqual(concavecut2.get_final_points_list(), [p5, int6, p4, int3, p3, int2, p2, int1, p1, p8, p7, int4, p6, int5])
            self.assertEqual(concavecut2.get_final_polygon_list(), [[10, 3, 11], [9, 2, 10, 11, 4, 5, 6, 12], [8, 1, 9, 12, 7, 13], [0, 8, 13]])
        except Exception as ex:
            self.fail()

