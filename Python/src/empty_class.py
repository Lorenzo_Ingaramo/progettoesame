import numpy as np
import scipy as sp
import math
from enum import Enum


class TypeIntersection(Enum):
    NOTINT = 0
    PROPERINT = 1
    BEGINVERTEXINT = 2  # intersezione nel vertice (inizio) del segmento
    ENDVERTEXINT = 3  # intersezione nel vertice (fine) del segmento

class IPoint:
    def set_vertex(self, v: int):
        pass

    def get_x(self) ->float:
        pass

    def get_y(self) -> float:
        pass

    def get_vertex(self) -> int:
        pass

    def set_type_intersection(self, typeint: TypeIntersection):
        pass

    def get_type_intersection(self) -> TypeIntersection:
        pass

class Point(IPoint):
    def __init__(self, x: float, y: float):
        self.__x = x
        self.__y = y
        self.__type = TypeIntersection.NOTINT

    def set_vertex(self, v: int):
        self.__numv = v

    def get_x(self):
        return self.__x

    def get_y(self):
        return self.__y

    def get_vertex(self):
        return self.__numv

    def __lt__(self, point):  # overload dell'operatore < per ordinare i punti in base al vertice
        return self.__numv < point.get_vertex()

    def __eq__(self, other):  # overload dell'operatore = per confrontare le liste di punti
        return (abs(self.__x - other.get_x()) < 1e-5) and (abs(self.__y - other.get_y()) < 1e-5)

    def set_type_intersection(self, typeint: TypeIntersection):
        self.__type = typeint

    def get_type_intersection(self):
        return self.__type


class Segment:
    def __init__(self, begin: IPoint, end: IPoint):
        if abs(begin.get_x() - end.get_x()) < 1e-6 and abs(begin.get_y() - end.get_y()) < 1e-6:
            raise Exception("segmento non creato: punti troppo vicini")
        self.__begin = begin  # punto di inizio
        self.__end = end  # punto di fine
        # vettore colonna che indica la direzione
        self.__direction = np.array([end.get_x() - begin.get_x(), end.get_y() - begin.get_y()])
        # vettore colonna che indica la normale alla direzione del segmento, uscente dal poligono
        self.__normal_direction = np.array([self.__direction[1], -self.__direction[0]])

    def get_direction(self):
        return self.__direction

    def get_normal_direction(self):
        return self.__normal_direction

    def get_begin(self) -> IPoint:
        return self.__begin

    def get_end(self) -> IPoint:
        return self.__end

class IPolygon:
    def get_points_list(self):
        pass

    def get_segments_list(self):
        pass

class Polygon(IPolygon):
    def __init__(self, points_list: list, vertices_list: list):
        self.__points_list = points_list  # lista dei punti (NON necessariamente ordinati) del poligono
        i: IPoint
        k: int = 0
        for i in self.__points_list:
            i.set_vertex(vertices_list[k])  # assegnazione del vertice ad ogni punto secondo la lista in input
            k += 1
        self.__points_list.sort()  # ordinamento dei punti in base al vertice (overload riga 32)
        self.__segments_list = []
        n = len(self.__points_list)
        k = 0
        j: int = 0
        for j in range(0, n):
            if k != n - 1:
                self.__segments_list.append(Segment(self.__points_list[j], self.__points_list[j + 1]))
                # creazione dei segmenti del poligono
                k += 1
            elif k == n - 1:
                self.__segments_list.append(Segment(self.__points_list[j], self.__points_list[0]))
                # creazione del segmento tra l'ultimo vertice e il primo vertice -> chiusura del poligono

    def get_points_list(self):
        return self.__points_list

    def get_segments_list(self):
        return self.__segments_list


class IntersectionPoint:
    def __init__(self):
        self.__edge: Segment
        self.__line: Segment
        # edge e line sono due segmenti: consideriamo edge un vero e proprio segmento (finito) e line una retta

    def set_tolerance_parallelism(self, toll: float):
        self.__tollParallelism = toll

    def set_tolerance_tangent(self, toll: float):
        self.__tollTangent = toll

    def compute_parallelism(self):
        edgeNorm = np.linalg.norm(self.__edge.get_direction())  # norma di edge
        lineNorm = np.linalg.norm(self.__line.get_direction())  # norma di line
        # controlliamo che line ed edge NON siano paralleli
        if abs(abs(self.__edge.get_direction()[0] / edgeNorm) - abs(
                self.__line.get_direction()[0] / lineNorm)) < self.__tollParallelism \
                and abs(abs(self.__edge.get_direction()[1] / edgeNorm) - abs(
            self.__line.get_direction()[1] / lineNorm)) < self.__tollParallelism:
            return True
        else:
            return False

    def compute_intersection(self, edge: Segment, line: Segment):
        self.__edge = edge
        self.__line = line
        edgeDirection = self.__edge.get_direction()
        lineDirection = self.__line.get_direction()
        if self.compute_parallelism() is True:  # verifica del NON parallelismo
            return TypeIntersection.NOTINT
        A = np.array([[edgeDirection[0], -lineDirection[0]], [edgeDirection[1], -lineDirection[1]]])
        # creazione della matrice per il calcolo dell'intersezione
        endEdge: IPoint = self.__edge.get_end()  # acquisizione del punto finale di edge
        xl = endEdge.get_x()
        yl = endEdge.get_y()
        endLine: IPoint = self.__line.get_end()  # acquisizione del punto finale di line
        xr = endLine.get_x()
        yr = endLine.get_y()
        b = np.array([[xl - xr], [yl - yr]])
        # creazione del vettore termine noto per il calcolo dell'intersezione
        x = np.linalg.solve(A, b)  # CALCOLO INTERSEZIONE

        # CLASSIFICAZIONE DEI PUNTI DI INTERSEZIONE e SETTING DEL TIPO DI INTERSEZIONE
        if x[0][0] > self.__tollTangent and x[0][0] < 1 - self.__tollTangent:
            self.__Found = Point(+xl - (edgeDirection[0]) * x[0][0],
                                 +yl - (edgeDirection[1]) * x[0][0])
            self.__Found.set_type_intersection(TypeIntersection.PROPERINT)  # Intersezione "propria"
            return TypeIntersection.PROPERINT
        elif abs(x[0][0]) < self.__tollTangent:
            edge.get_end().set_type_intersection(
                TypeIntersection.ENDVERTEXINT)  # Intersezione nel vertice (fine) del segmento
            return TypeIntersection.ENDVERTEXINT
        elif abs(x[0][0] - 1) < self.__tollTangent:
            edge.get_begin().set_type_intersection(
                TypeIntersection.BEGINVERTEXINT)  # Intersezione nel vertice (inizio) del segmento
            return TypeIntersection.BEGINVERTEXINT
        else:
            return TypeIntersection.NOTINT  # NON intersezione

    def get_found_point(self):
        return self.__Found


class PolygonCut:
    def __init__(self):
        self.__polygons_list = []  # lista dei poligoni creati dal taglio
        self.__found_points_list = []  # lista di punti contenente i punti "originari" e le eventuali intersezioni
        self.__intersection_segments = []  # lista di segmenti interessati dalle intersezioni

    def cut(self, points_list: list, vertices_list: list, segment_list: list):
        self.__polygon = Polygon(points_list,
                                 vertices_list)  # creazione del poligono a partire dai punti e dai relativi vertici
        # creazione, a partire dai due punti, del segmento che andrà a tagliare il poligono
        self.__segment = Segment(segment_list[0], segment_list[1])
        intersections_list = []  # lista dei soli punti di intersezione
        segments_list = self.__polygon.get_segments_list()  # copia della lista di segmenti del poligono
        i: Segment
        l = len(points_list)
        intPoint = IntersectionPoint()
        intPoint.set_tolerance_parallelism(1e-6)
        intPoint.set_tolerance_tangent(1e-6)
        for i in segments_list:
            self.__found_points_list.append(i.get_begin())  # creazione della lista di tutti i punti
            type = intPoint.compute_intersection(i, self.__segment)  # identificazione del tipo di punto

            # iINTERSEZIONE PROPRIA
            if type is TypeIntersection.PROPERINT:
                intPoint.get_found_point().set_vertex(l)  # assegnazione vertice
                self.__found_points_list.append(intPoint.get_found_point())  # inserimento nella lista di tutti i punti
                intersections_list.append(intPoint.get_found_point())  # inserimento nella lista delle intersezioni
                l += 1
                self.__intersection_segments.append(i)  # inserimento del segmento nella lista dei segmenti intersezione

            # INTERSEZIONE NEL VERTICE (INIZIO) DEL SEGMENTO
            if type is TypeIntersection.BEGINVERTEXINT:
                intersections_list.append(i.get_begin())  # inserimento nella lista delle intersezioni
                self.__intersection_segments.append(i)  # inserimento del segmento nella lista dei segmenti intersezione

        j: IPoint
        k: int = 0
        for j in self.__found_points_list:

            if j.get_type_intersection() is TypeIntersection.PROPERINT:
                k += 1

            elif j.get_type_intersection() is TypeIntersection.BEGINVERTEXINT or j.get_type_intersection() is TypeIntersection.ENDVERTEXINT:

                if len(intersections_list) is 1:
                    # se l'intersezione al vertice è l'unica intersezione ottenuta, allora verrà considerata una NON intersezione
                    j.set_type_intersection(TypeIntersection.NOTINT)

                else:
                    # se l'intersezione al vertice NON è l'unica intersezione ottenuta e il segmento è interno al
                    # poligono, allora l'intersezione è propria
                    if k < (len(intersections_list) - 1) and self.check_inside(j, intersections_list[k + 1]) is True:
                        j.set_type_intersection(TypeIntersection.PROPERINT)
                    elif k > 0 and self.check_inside(j, intersections_list[k - 1]) is True:
                        j.set_type_intersection(TypeIntersection.PROPERINT)
                    else:
                        j.set_type_intersection(TypeIntersection.NOTINT)
                k += 1
        if self.__segment.get_begin().get_type_intersection() is TypeIntersection.NOTINT:
            self.__segment.get_begin().set_vertex(l)
        if self.__segment.get_end().get_type_intersection() is TypeIntersection.NOTINT:
            self.__segment.get_end().set_vertex(l + 1)
        sending_list = []
        j: IPoint
        for j in self.__found_points_list:
            sending_list.append(j)
            # copia della lista di tutti i punti da mandare in input alla funzinoe di costruzione dei poligoni
        self.polygons_construction(sending_list, self.__found_points_list[0])

    def get_final_points_list(self):
        return self.__found_points_list

    def get_final_polygon_list(self):
        return self.__polygons_list

    def polygons_construction(self, sending_list_copy: list, initialPoint: IPoint):
        points_list = sending_list_copy  # lista di tutti i punti
        polygon_points_list = []  # lista di punti del poligono in costruzione
        polygon_vertices_list = []  # lista di vertici del poligono in costruzione
        i: int
        # inserimento del primo punto nella lista specifica del poligono ed eliminazione da quella generale
        polygon_points_list.append(points_list[0])
        points_list.pop(0)

        while len(points_list) is not 0:
            # se abbiamo una NON intersezione, allora inseriamo nella lista specifica ed eliminiamo dalla lista generale
            if points_list[0].get_type_intersection() is TypeIntersection.NOTINT:
                polygon_points_list.append(points_list[0])
                points_list.pop(0)

            else:
                # altrimenti inseriamo solo nella lista specifica
                polygon_points_list.append(points_list[0])
                # CASO PART.: se il primo punto è il primo punto del poligono originario,
                # allora richiamiamo subito la funzione di costruzione
                if initialPoint.get_vertex() is 0:
                    self.polygons_construction(points_list, points_list[0])
                    polygon_points_list.append(points_list[0])
                    points_list.pop(0)
                else:
                    # altrimenti, verifichiamo la posizione del segmento rispetto al poligono
                    if self.check_inside(initialPoint, points_list[0]) is False:
                        # se il segmento risulta essere esterno al poligono, allora richiamiamo la funzione di costruzione
                        self.polygons_construction(points_list, points_list[0])
                        polygon_points_list.append(points_list[0])
                        points_list.pop(0)
                    else:
                        # Verifico l'inserimento dei vertici del segmento tagliante
                        t_finale = (initialPoint.get_x() - self.__segment.get_end().get_x()) / \
                                   (self.__segment.get_begin().get_x() - self.__segment.get_end().get_x())
                        t_iniziale = (polygon_points_list[
                                          len(polygon_points_list) - 1].get_x() - self.__segment.get_end().get_x()) / (
                                             self.__segment.get_begin().get_x() - self.__segment.get_end().get_x())
                        # Verificare quale è maggiore e quale minore:
                        punto_iniziale = self.__segment.get_begin()
                        punto_finale = self.__segment.get_end()
                        if t_finale < t_iniziale:
                            tmp_t: float = t_finale
                            t_finale = t_iniziale
                            t_iniziale = tmp_t
                            # Punti entrambi sul segmento interno , scambio i due punti, per questioni di verso antiorario
                            if t_finale > 1 + 1e-5 and t_iniziale < -1e-5:
                                tmp_point: IPoint = punto_finale
                                punto_finale = punto_iniziale
                                punto_iniziale = tmp_point

                            # Verifico quale punto punto di quelli de segmento è interno al poligono
                        if t_finale > 1e-5 and t_iniziale < - 1e-5:  # punto finale in mezzo
                            polygon_points_list.append(punto_iniziale)
                            points_list.insert(0, punto_iniziale)
                            points_list[1].set_type_intersection(TypeIntersection.NOTINT)

                        if t_finale > 1 + 1e-5 and t_iniziale < 1 - 1e-5:  # punto finale in mezzo
                            polygon_points_list.append(punto_finale)
                            points_list.insert(0, punto_finale)
                            points_list[1].set_type_intersection(TypeIntersection.NOTINT)

                        break
        k: Point
        # ottenuta la lista (ordinata) di punti del poligono, creiamo la lista di vertici relativi ai punti
        for k in polygon_points_list:
            polygon_vertices_list.append(k.get_vertex())
        self.__polygons_list.append(polygon_vertices_list)

    def check_inside(self, p1: IPoint, p2: IPoint):
        p3 = Point(p2.get_x(), p2.get_y())
        p4 = Point(p1.get_x(), p1.get_y())
        segment = Segment(p4, p3)  # creazione segmento
        flag: bool = False
        i: int = 0
        # creazione punto medio
        midpoint = Point(p3.get_x() - segment.get_direction()[0] * 0.5,
                         p3.get_y() - segment.get_direction()[1] * 0.5)
        intpoint = IntersectionPoint()
        intpoint.set_tolerance_tangent(1e-6)
        intpoint.set_tolerance_parallelism(1e-6)
        j: Segment
        for j in self.__intersection_segments:
            # se otteniamo un'intersezione interna al segmento, allora il segmento NON potrà essere
            # interno -> dunque viene considerato esterno
            if intpoint.compute_intersection(segment, j) is TypeIntersection.PROPERINT:
                return flag
        for i in range(0, len(self.__intersection_segments) - 1):
            # res1 è il prodotto scalare tra la direzione del segmento e la direzione normale al segmento in posizione i
            res1 = np.inner(segment.get_direction(), self.__intersection_segments[i].get_normal_direction())
            # res2 è il prodotto scalare tra l'opposto della direzione del segmento e la dir. normale al segmento in posizione i+1
            res2 = np.inner(-segment.get_direction(), self.__intersection_segments[i + 1].get_normal_direction())

            if (res1 < 0) and (res2 < 0):
                # s1, s2 sono segmenti che partono dai punti iniziali dei segmenti del poligono e arrivano al punto medio
                # del segmento in questione
                s1 = Segment(self.__intersection_segments[i].get_begin(), midpoint)
                s2 = Segment(self.__intersection_segments[i + 1].get_begin(), midpoint)
                # ext1 è il prodotto vettoriale (terza componente) tra il segmento in posizione i e il
                # segmento s1
                ext1 = self.__intersection_segments[i].get_direction()[0] * s1.get_direction()[1] - \
                       self.__intersection_segments[i].get_direction()[1] * s1.get_direction()[0]
                # ext2 è il prodotto vettoriale (terza componente) tra il segmento in posizione i+1 e il
                # segmento s2
                ext2 = self.__intersection_segments[i + 1].get_direction()[0] * s2.get_direction()[1] - \
                       self.__intersection_segments[i + 1].get_direction()[1] * s2.get_direction()[0]
                # se il punto medio risulta essere interno al poligono, allora il segmento è interno al poligono
                if ext1 > 0 and ext2 > 0:
                    flag = True
                    break
        return flag
